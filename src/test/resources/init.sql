drop table if exists attempt, student_activity, profile, activity, course, user, role cascade;

create table if not exists role
(
    id bigint auto_increment
        primary key,
    name varchar(64) not null,
    constraint UK_8sewwnpamngi6b1dwaa88askk
        unique (name)
);

create table if not exists user
(
    id bigint auto_increment
        primary key,
    creation_time datetime null,
    update_time datetime null,
    email varchar(64) not null,
    is_deleted bit null,
    username varchar(64) not null,
    constraint UK_ob8kqyqqgmefl0aco34akdtpe
        unique (email),
    constraint UK_sb8bbouer5wak8vyiiy4pf2bx
        unique (username)
);

create table course
(
    id bigint auto_increment
        primary key,
    creation_time datetime null,
    update_time datetime null,
    is_deleted bit null,
    name varchar(64) not null,
    creator_id bigint null,
    end_date datetime null,
    isbn bigint not null,
    start_date datetime null,
    external_id varchar(255) not null,
    constraint UKi1crw5mr1vn2s1effdkjhima8
        unique (name, creator_id),
    constraint FK1isig5e63r0u4klsxr1qfk7dt
        foreign key (creator_id) references user (id)
);

create table if not exists activity
(
    id bigint auto_increment
        primary key,
    creation_time datetime null,
    update_time datetime null,
    content_link varchar(255) null,
    is_deleted bit null,
    name varchar(64) not null,
    course_id bigint null,
    max_mark bigint null,
    constraint FKad1q04q5dxv9lyur6pyighso
        foreign key (course_id) references course (id)
);

create table if not exists profile
(
    id bigint auto_increment
        primary key,
    state varchar(255) null,
    course_id bigint null,
    role_id bigint null,
    user_id bigint null,
    creation_time datetime null,
    update_time datetime null,
    constraint FKawh070wpue34wqvytjqr4hj5e
        foreign key (user_id) references user (id),
    constraint FKo678bnfsd4kqra9pc48glmdra
        foreign key (role_id) references role (id),
    constraint FKtn4h56orm44m80bwe4cv5mm7h
        foreign key (course_id) references course (id)
);

create table if not exists student_activity
(
    id bigint auto_increment
        primary key,
    creation_time datetime null,
    update_time datetime null,
    status varchar(255) null,
    activity_id bigint null,
    final_mark bigint null,
    profile_id bigint null,
    constraint FK827jp7b3awvt5mf1sktp2gb72
        foreign key (profile_id) references profile (id),
    constraint FKqxjjlxcoy2nw602qvagbwj4kr
        foreign key (activity_id) references activity (id)
);

create table if not exists attempt
(
    id bigint not null
        primary key,
    mark bigint not null,
    user_activity_id bigint null,
    constraint FKi29888hqoar1xln83k0gep9jy
        foreign key (user_activity_id) references student_activity (id)
);

DELETE FROM attempt;
DELETE FROM student_activity;
DELETE FROM profile;
DELETE FROM role;
DELETE FROM activity;
DELETE FROM course;
DELETE FROM user;


insert into role (id, name)
values  (1, 'student'),
        (2, 'teacher');

insert into user (id, creation_time, update_time, email, is_deleted, username)
values  (1, '2021-06-29 18:45:45', '2021-06-29 18:45:45', 'nick@gmail.com', false, 'Nick'),
        (2, '2021-06-29 18:45:45', '2021-06-29 18:45:45', 'jhon@gmail.com', false, 'Jhon'),
        (3, '2021-06-29 18:45:45', '2021-06-29 18:45:45', 'piter@gmail.com', false, 'Piter'),
        (4, '2021-06-29 18:45:45', '2021-06-29 18:45:45', 'kate@gmail.com', false, 'Kate'),
        (5, '2021-06-29 18:45:45', '2021-06-29 18:45:45', 'barbara@gmail.com', false, 'Barbara'),
        (6, '2021-06-29 18:45:45', '2021-06-29 18:45:45', 'will@gmail.com', false, 'Will'),
        (7, '2021-07-01 13:29:50', '2021-07-01 13:29:50', 'jess@gmail.com', false, 'Jess'),
        (8, '2021-07-01 13:48:25', '2021-07-01 13:48:25', 'Tim@gmail.com', false, 'Tim'),
        (9, '2021-07-01 17:09:38', '2021-07-01 14:11:58', 'Shon@gmail.com', false, 'Shon1'),
        (10, '2021-07-08 13:04:02', '2021-07-08 13:04:02', 'test@gmail.com', false, 'Test'),
        (11, '2021-07-08 13:04:23', '2021-07-08 13:04:23', 'ford@gmail.com', false, 'Ford'),
        (12, '2021-07-12 09:20:35', '2021-07-12 09:20:35', 'jan@gmail.com', false, 'Jennet');

INSERT INTO course (creation_time, update_time, is_deleted, name, creator_id, end_date, isbn, start_date, external_id) VALUES ('2021-06-29 18:46:04', '2021-06-29 18:46:04', false, 'Math', 2, '2021-07-21 08:45:23', 99999999, '2021-06-29 18:46:04', '3fa85f64-5717-4562-b3fc-2c963f66afa6');
INSERT INTO course (creation_time, update_time, is_deleted, name, creator_id, end_date, isbn, start_date, external_id) VALUES ('2021-06-29 18:46:04', '2021-06-29 18:46:04', false, 'English', 2, '2022-07-21 08:45:23', 99999991, '2021-06-29 18:46:04', '3fa85f64-5717-4562-b3fc-2c963f66afa5');
INSERT INTO course (creation_time, update_time, is_deleted, name, creator_id, end_date, isbn, start_date, external_id) VALUES ('2021-07-01 15:51:37', '2021-07-05 13:54:50', false, 'BG', 8, '2021-07-21 08:45:23', 99999992, '2021-06-29 18:46:04', '3fa85f64-5717-4562-b3fc-2c963f66afa4');
INSERT INTO course (creation_time, update_time, is_deleted, name, creator_id, end_date, isbn, start_date, external_id) VALUES ('2021-07-12 09:40:15', '2021-07-12 09:40:15', false, 'History', 8, '2021-07-21 08:45:23', 99999993, '2021-06-29 18:46:04', '3fa85f64-5717-4562-b3fc-2c963f66afa3');
INSERT INTO course (creation_time, update_time, is_deleted, name, creator_id, end_date, isbn, start_date, external_id) VALUES ('2021-07-18 18:14:17', '2021-07-18 18:14:17', false, 'Dimas', 3, '2021-07-02 08:45:23', 99999994, '2021-06-29 18:46:04', '3fa85f64-5717-4562-b3fc-2c963f66afa1');
INSERT INTO course (creation_time, update_time, is_deleted, name, creator_id, end_date, isbn, start_date, external_id) VALUES ('2021-07-21 08:45:23', '2021-07-21 08:45:23', false, 'BG', 2, '2021-07-21 08:45:23', 99999995, '2021-06-29 18:46:04', '3fa85f64-5717-4562-b3fc-2c963f66afa2');

insert into profile (id, state, course_id, role_id, user_id, creation_time, update_time)
values  (1, 'ACTIVE', 1, 2, 2, '2021-06-29 18:46:04', '2021-06-29 18:46:04'),
        (2, 'ACTIVE', 2, 2, 8, '2021-06-29 18:46:04', '2021-06-29 18:46:04'),
        (3, 'ACTIVE', 3, 2, 2, '2021-06-29 18:46:04', '2021-06-29 18:46:04'),
        (4, 'ACTIVE', 1, 1, 1, '2021-06-29 18:46:04', '2021-06-29 18:46:04');

INSERT INTO activity (id, creation_time, update_time, content_link, max_mark,is_deleted, name, course_id) VALUES (1, '2021-07-02 07:50:45', '2021-07-02 07:50:45', 'link_to_test_practical',100,false, 'Practical homework', 1);

INSERT INTO student_activity (id, creation_time, update_time, status,final_mark ,activity_id, profile_id) VALUES (1, '2021-06-29 18:53:16', '2021-06-29 18:53:19', 'IN_PROGRESS', null ,1, 1);
INSERT INTO student_activity (id, creation_time, update_time, status,final_mark ,activity_id, profile_id) VALUES (2, '2021-06-29 18:53:16', '2021-06-29 18:53:19', 'NOT_STARTED', null ,1, 2);

INSERT INTO attempt (id, mark, user_activity_id) VALUES (1, 70, 1);

ALTER TABLE student_activity AUTO_INCREMENT=3;
ALTER TABLE profile AUTO_INCREMENT=5;
ALTER TABLE course AUTO_INCREMENT=5;
ALTER TABLE attempt AUTO_INCREMENT=3;
ALTER TABLE activity AUTO_INCREMENT=2;
ALTER TABLE user AUTO_INCREMENT=13;