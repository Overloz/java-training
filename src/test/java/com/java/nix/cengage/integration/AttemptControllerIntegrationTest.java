package com.java.nix.cengage.integration;

import com.java.nix.cengage.controller.AttemptController;
import com.java.nix.cengage.entity.dto.AttemptDto;
import com.java.nix.cengage.exception.validation.AttemptValidationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("test")
@SpringBootTest
public class AttemptControllerIntegrationTest extends TestContainerConfiguration {

    private final AttemptController attemptController;

    @Autowired
    public AttemptControllerIntegrationTest(AttemptController attemptController) {
        this.attemptController = attemptController;
    }

    @Test
    @Transactional
    public void addAttemptWhileStudentActivityInProgressAndGetExceptionTest() {
        assertThrows(AttemptValidationException.class,
                () -> attemptController.createAttempt(new AttemptDto(1L, 1L, 1L)));
    }

}
