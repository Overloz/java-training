package com.java.nix.cengage.integration;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.cache.Cache;
import com.java.nix.cengage.repositories.mongo.CacheRepository;
import com.java.nix.cengage.repositories.mysql.ActivityRepository;
import com.java.nix.cengage.service.ActivityService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ActivityServiceIntegrationTest extends TestContainerConfiguration {

    private final static String TEST_STRING = "test";

    private final ActivityService activityService;

    private final ActivityRepository activityRepository;

    private final CacheRepository cacheRepository;

    @Autowired
    public ActivityServiceIntegrationTest(ActivityService activityService,
            ActivityRepository activityRepository, CacheRepository cacheRepository) {
        this.activityService = activityService;
        this.activityRepository = activityRepository;
        this.cacheRepository = cacheRepository;
    }

    @BeforeEach
    public void preInit() {
        Course course = Course.builder().id(1L).build();
        Activity activity = Activity.builder().id(1L).name(TEST_STRING).course(course)
                .contentLink(TEST_STRING).build();
        Cache cache = new Cache(1L, Set.of(activity));

        cacheRepository.save(cache);
    }

    @AfterEach
    public void afterTest() {
        cacheRepository.deleteById(1L);
    }

    @Test
    @Transactional
    public void addActivityToCourseSuccessfullyTest() {
        Course course = Course.builder().id(1L).build();
        Activity activityToSave = Activity.builder().name(TEST_STRING).contentLink(TEST_STRING).course(course)
                .maxMark(1L).build();

        List<Cache> cacheBeforeTest = cacheRepository.findAll();
        List<Activity> activitiesBeforeTest = activityRepository.findAll();

        activityService.insert(activityToSave);

        List<Activity> activitiesAfterTest = activityRepository.findAll();
        List<Cache> cacheAfterTest = cacheRepository.findAll();

        assertEquals(cacheBeforeTest.size() - 1, cacheAfterTest.size());

        assertEquals(activitiesBeforeTest.size() + 1, activitiesAfterTest.size());

        Activity savedActivity = activitiesAfterTest.get(activitiesAfterTest.size() - 1);

        assertEquals(savedActivity, activityToSave);

    }

    @Test
    @Transactional
    public void deleteActivityAndGetCacheDeletedTest() {
        List<Cache> cacheBeforeTest = cacheRepository.findAll();
        Activity activityToDelete = activityRepository.getById(1L);
        List<Activity> activitiesBeforeTest = activityRepository.findAll();

        activityService.deleteById(1L);

        List<Cache> cacheAfterTest = cacheRepository.findAll();
        List<Activity> activitiesAfterTest = activityRepository.findAll();

        assertEquals(activitiesBeforeTest.size() - 1, activitiesAfterTest.size());
        assertEquals(cacheBeforeTest.size() - 1, cacheAfterTest.size());
        assertTrue(activitiesBeforeTest.contains(activityToDelete));
        assertFalse(activitiesAfterTest.contains(activityToDelete));
    }

    @Test
    @Transactional
    public void updateActivityAndGetCacheDeletedTest() {
        Long mark = 100L;
        Activity activityToUpdate = Activity.builder()
                .id(1L)
                .name(TEST_STRING)
                .contentLink(TEST_STRING)
                .maxMark(mark)
                .isDeleted(false)
                .build();

        List<Cache> cacheBeforeTest = cacheRepository.findAll();

        activityService.update(activityToUpdate);

        List<Cache> cacheAfterTest = cacheRepository.findAll();
        Activity updatedActivity = activityRepository.getById(1L);

        assertEquals(cacheBeforeTest.size() - 1, cacheAfterTest.size());
        assertEquals(updatedActivity, activityToUpdate);
    }
}
