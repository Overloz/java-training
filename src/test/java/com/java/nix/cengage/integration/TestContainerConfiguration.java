package com.java.nix.cengage.integration;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;

@TestConfiguration
@SpringBootTest
public class TestContainerConfiguration {

    @Container
    public static MongoDBContainer container;

    @Container
    public static MySQLContainer<?> MY_SQL_CONTAINER;

    @DynamicPropertySource
    static void mysqlProperties(DynamicPropertyRegistry registry) {
        String mongoUrl = String
                .format("mongodb://%s:%s/%s", container.getHost(), container.getFirstMappedPort(),
                        "test");

        registry.add("spring.datasource.url", MY_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.password", MY_SQL_CONTAINER::getPassword);
        registry.add("spring.datasource.username", MY_SQL_CONTAINER::getUsername);
        registry.add("spring.data.mongodb.uri", () -> mongoUrl);
    }

    static {
        MY_SQL_CONTAINER = new MySQLContainer<>("mysql:5.5").withDatabaseName("TEST")
                .withUsername("root").withPassword("303030").withInitScript("init.sql")
                .withExposedPorts(80).withReuse(true);
        MY_SQL_CONTAINER.start();
        container = new MongoDBContainer(DockerImageName.parse("mongo:4.4.6")).withReuse(true);
        container.start();
        System.out.println(container.getReplicaSetUrl());
    }
}


