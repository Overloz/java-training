package com.java.nix.cengage.integration;

import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.entity.enums.ActivityStatus;
import com.java.nix.cengage.repositories.mysql.StudentActivityRepository;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class StudentActivityServiceIntegrationTest extends TestContainerConfiguration {

    private final StudentActivityService studentActivityService;

    private final StudentActivityRepository studentActivityRepository;

    @Autowired
    public StudentActivityServiceIntegrationTest(StudentActivityService studentActivityService,
            StudentActivityRepository studentActivityRepository) {
        this.studentActivityService = studentActivityService;
        this.studentActivityRepository = studentActivityRepository;
    }

    @Test
    public void finalizeAllStudentActivitiesTest() {
        List<StudentActivity> beforeTest = studentActivityRepository.findAllByActivityId(1L);
        StudentActivity studentActivityBefore = beforeTest.get(0);

        studentActivityService.finalizeAllByActivityId(1L);

        List<StudentActivity> afterTest = studentActivityRepository.findAllByActivityId(1L);

        StudentActivity studentActivityAfter = afterTest.get(0);

        assertSame(studentActivityAfter.getStatus(), ActivityStatus.FINALIZED);
        assertNotSame(studentActivityAfter.getStatus(), studentActivityBefore.getStatus());
    }

    @Test
    public void getAllByActivityId() {
        List<StudentActivity> activitiesFromRepository = studentActivityRepository
                .findAllByActivityId(1L);
        List<StudentActivity> activitiesFromService = studentActivityService.getAllByActivityId(1L);

        assertEquals(activitiesFromService,activitiesFromRepository);
    }
}
