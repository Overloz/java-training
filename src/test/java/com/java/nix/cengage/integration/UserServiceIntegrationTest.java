package com.java.nix.cengage.integration;

import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.repositories.mysql.ProfileRepository;
import com.java.nix.cengage.repositories.mysql.UserRepository;
import com.java.nix.cengage.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserServiceIntegrationTest extends TestContainerConfiguration {

    private final UserService userService;

    private final UserRepository userRepository;

    private final ProfileRepository profileRepository;

    @Autowired
    public UserServiceIntegrationTest(UserService userService, UserRepository userRepository,
            ProfileRepository profileRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
    }

    @Test
    @Transactional
    public void addUserSuccessfullyTest() {
        String username = "user";
        String email = "user@gmail.com";
        User userToSave = User.builder().username(username).email(email).build();

        List<User> beforeTest = userRepository.findAll();

        userService.saveUser(userToSave);

        List<User> afterTest = userRepository.findAll();

        assertEquals(beforeTest.size() + 1, afterTest.size());

        User savedUser = afterTest.get(afterTest.size() - 1);

        assertEquals(userToSave, savedUser);
    }

    @Test
    @Transactional
    public void deleteUserSuccessfullyTest() {

        User userToDelete = userRepository.getById(1L);

        List<Profile> profilesUserBeforeTest = profileRepository.findAllByUserId(1L);

        List<User> usersBeforeTest = userRepository.findAll();

        userService.deleteById(1L);

        List<User> usersAfterTest = userRepository.findAll();

        List<Profile> profilesUserAfterTest = profileRepository.findAllByUserId(1L);

        assertEquals(usersBeforeTest.size() - 1, usersAfterTest.size());
        assertEquals(0, profilesUserAfterTest.size());
        assertFalse(profilesUserBeforeTest.isEmpty());
        assertTrue(usersBeforeTest.contains(userToDelete));
        assertFalse(usersAfterTest.contains(userToDelete));

    }
}
