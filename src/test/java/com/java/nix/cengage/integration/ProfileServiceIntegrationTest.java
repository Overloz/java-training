package com.java.nix.cengage.integration;

import com.java.nix.cengage.entity.*;
import com.java.nix.cengage.exception.search.CourseNotFoundException;
import com.java.nix.cengage.exception.search.UserNotFoundException;
import com.java.nix.cengage.repositories.mysql.ProfileRepository;
import com.java.nix.cengage.repositories.mysql.StudentActivityRepository;
import com.java.nix.cengage.service.ProfileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.java.nix.cengage.entity.enums.ProfileRole.TEACHER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ProfileServiceIntegrationTest extends TestContainerConfiguration {

    private final ProfileService profileService;

    private final ProfileRepository profileRepository;

    private final StudentActivityRepository studentActivityRepository;

    @Autowired
    public ProfileServiceIntegrationTest(ProfileService profileService,
            ProfileRepository profileRepository,
            StudentActivityRepository studentActivityRepository) {
        this.profileService = profileService;
        this.profileRepository = profileRepository;
        this.studentActivityRepository = studentActivityRepository;
    }

    @Test
    @Transactional
    public void addStudentToCourseSuccessfullyTest() {
        long userIdToBeSaved = 9L;

        User user = User.builder().id(userIdToBeSaved).build();
        Course course = Course.builder().id(1L).build();
        Role role = new Role(1L,"");

        Profile profileToSave = Profile.builder().user(user).course(course).role(role).build();
        List<StudentActivity> activitiesBeforeTest = studentActivityRepository.findAll();
        List<Profile> profilesBeforeTest = profileRepository.findAll();

        profileService.insertStudent(profileToSave);

        List<StudentActivity> activitiesAfterTest = studentActivityRepository.findAll();
        List<Profile> profilesAfterTest = profileRepository.findAll();


        assertEquals(activitiesBeforeTest.size() + 1, activitiesAfterTest.size());
        assertEquals(profilesBeforeTest.size() + 1, profilesAfterTest.size());

        Profile savedProfile = profilesAfterTest.get(profilesAfterTest.size() - 1);

        assertEquals(profileToSave.getCourse().getId(),savedProfile.getCourse().getId());
        assertEquals(profileToSave.getRole().getId(),savedProfile.getRole().getId());
        assertEquals(profileToSave.getUser().getId(),savedProfile.getUser().getId());
    }

    @Test
    @Transactional
    public void createProfileWithNotExistingUserAndGetUserNotFoundExceptionTest(){
        long userIdToBeSaved = 100L;

        User user = User.builder().id(userIdToBeSaved).build();
        Course course = Course.builder().id(1L).build();
        Role role = TEACHER.getRole();

        Profile profileToSave = Profile.builder().user(user).course(course).role(role).build();

        assertThrows(UserNotFoundException.class, () -> profileService.insert(profileToSave));
    }

    @Test
    @Transactional
    public void addUserToNotExistingCourseAndGetCourseNotFoundExceptionTest(){
        long courseId = 100L;

        User user = User.builder().id(1L).build();
        Course course = Course.builder().id(courseId).build();
        Role role = TEACHER.getRole();

        Profile profileToSave = Profile.builder().user(user).course(course).role(role).build();

        assertThrows(CourseNotFoundException.class, () -> profileService.insert(profileToSave));
    }
}
