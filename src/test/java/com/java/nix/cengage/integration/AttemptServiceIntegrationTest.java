package com.java.nix.cengage.integration;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.exception.validation.AttemptValidationException;
import com.java.nix.cengage.repositories.mysql.AttemptRepository;
import com.java.nix.cengage.service.AttemptService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest
public class AttemptServiceIntegrationTest extends TestContainerConfiguration {

    private final AttemptService attemptService;
    private final AttemptRepository attemptRepository;

    @Autowired
    public AttemptServiceIntegrationTest(AttemptService attemptService,
            AttemptRepository attemptRepository) {
        this.attemptService = attemptService;
        this.attemptRepository = attemptRepository;
    }

    @Test
    @Transactional
    public void addAttemptsToStudentActivityWithMarkLessThanZeroWithExceptionTest() {
        long maxMark = 100L;
        Activity activity = Activity.builder().maxMark(maxMark).build();
        StudentActivity studentActivity = StudentActivity.builder().id(1L).activity(activity)
                .build();

        Assertions.assertThrows(AttemptValidationException.class,
                () -> attemptService.insert(new Attempt(1L, -1L, studentActivity)));
    }

    @Test
    @Transactional
    public void addAttemptsToStudentActivityWithMarkBiggerThanMaxWithExceptionTest() {
        long maxMark = 1L;
        long mark = 1000L;
        Activity activity = Activity.builder().maxMark(maxMark).build();
        StudentActivity studentActivity = StudentActivity.builder().id(1L).activity(activity)
                .build();

        Assertions.assertThrows(AttemptValidationException.class,
                () -> attemptService.insert(new Attempt(1L, mark, studentActivity)));
    }

    @Test
    @Transactional
    public void addAttemptSuccessfullyTest() {
        long attemptId = 2L;
        long maxMark = 100L;
        Activity activity = Activity.builder().maxMark(maxMark).build();
        StudentActivity studentActivity = StudentActivity.builder().id(2L).activity(activity)
                .build();

        Attempt attemptToSave = new Attempt(attemptId, 1L, studentActivity);

        List<Attempt> attemptsBeforeTest = attemptRepository.findAll();


        attemptService.insert(attemptToSave);

        List<Attempt> attemptsAfterTest = attemptRepository.findAll();
        Attempt savedAttempt = attemptsAfterTest.get(attemptsAfterTest.size() - 1);

        assertEquals(attemptsBeforeTest.size() + 1, attemptsAfterTest.size());
        assertEquals(attemptToSave, savedAttempt);

    }

}
