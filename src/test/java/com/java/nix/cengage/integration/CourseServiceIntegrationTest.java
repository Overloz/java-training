package com.java.nix.cengage.integration;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.entity.cache.Cache;
import com.java.nix.cengage.exception.search.CourseNotFoundException;
import com.java.nix.cengage.repositories.mongo.CacheRepository;
import com.java.nix.cengage.repositories.mysql.CourseRepository;
import com.java.nix.cengage.service.CourseService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CourseServiceIntegrationTest extends TestContainerConfiguration {

    private final static String TEST_STRING = "test";

    private final CourseService courseService;
    private final CourseRepository courseRepository;
    private final CacheRepository cacheRepository;

    @Autowired
    public CourseServiceIntegrationTest(CourseService courseService,
            CourseRepository courseRepository, CacheRepository cacheRepository) {
        this.courseService = courseService;
        this.courseRepository = courseRepository;
        this.cacheRepository = cacheRepository;
    }

    @BeforeEach
    public void preInit() {
        Course course = Course.builder().id(1L).build();
        Activity activity = Activity.builder().id(1L).name(TEST_STRING).course(course)
                .contentLink(TEST_STRING).build();
        Cache cache = new Cache(1L, Set.of(activity));

        cacheRepository.save(cache);
    }

    @AfterEach
    public void afterTest() {
        cacheRepository.deleteById(1L);
    }

    @Test
    @Transactional
    public void addCourseSuccessfullyTest() {
        Long isbn = 99992329L;
        User user = User.builder().id(1L).build();
        Course courseToSave = Course.builder().isbn(isbn).name(TEST_STRING).creator(user).build();
        List<Course> coursesBeforeTest = courseRepository.findAll();

        courseService.insert(courseToSave);

        List<Course> coursesAfterTest = courseRepository.findAll();

        assertEquals(coursesBeforeTest.size() + 1, coursesAfterTest.size());

        Course savedCourse = coursesAfterTest.get(coursesAfterTest.size() - 1);

        assertEquals(savedCourse, courseToSave);
    }

    @Test
    @Transactional
    public void deleteCourseAndGetCacheDeletedTest() {
        long courseIdToDelete = 4L;

        Course courseToDelete = courseRepository.getById(4L);
        List<Course> coursesBeforeTest = courseRepository.findAll();

        courseService.deleteById(courseIdToDelete);

        List<Course> coursesAfterTest = courseRepository.findAll();

        assertEquals(coursesBeforeTest.size() - 1, coursesAfterTest.size());
        assertFalse(coursesAfterTest.contains(courseToDelete));
        assertTrue(coursesBeforeTest.contains(courseToDelete));

    }

    @Test
    @Transactional
    public void deleteCourseWithNotExistingIdAndGetNotFoundExceptionTest() {
        long courseIdToDelete = 1000L;

        assertThrows(CourseNotFoundException.class,
                () -> courseService.deleteById(courseIdToDelete));
    }

    @Test
    @Transactional
    public void updateCourseWithNotExistingIdAndGetNotFoundExceptionTest() {
        long courseIdToUpdate = 100L;
        Course courseToUpdate = Course.builder().id(courseIdToUpdate).build();

        assertThrows(CourseNotFoundException.class, () -> courseService.update(courseToUpdate));
    }
}
