package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.entity.enums.ActivityStatus;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class StudentActivityControllerTest {

    @Mock
    private StudentActivityService studentActivityService;

    private MockMvc mockMvc;

    private StudentActivity studentActivity;

    @BeforeEach
    void setup() {
        this.studentActivity = createStudentActivity();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                new StudentActivityController(studentActivityService))
                .build();
    }

    @Test
    public void finalizeAllByActivityIdTest() throws Exception {
        mockMvc.perform(put("/studentActivity/activity/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(studentActivityService, times(1)).finalizeAllByActivityId(1L);
    }

    @Test
    public void getAllByActivityIdTest() throws Exception {
        lenient().when(studentActivityService.getAllByActivityId(1L)).thenReturn(List.of(studentActivity));

        mockMvc.perform(get("/studentActivity/activity/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(studentActivityService, times(1)).getAllByActivityId(1L);
    }

    @Test
    public void getActivitiesWithMarksTest() throws Exception {
        doReturn(List.of(studentActivity)).when(studentActivityService).getResults(1L, 1L);

        mockMvc.perform(get("/studentActivity/1/student/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private StudentActivity createStudentActivity() {
        Profile profile = new Profile();
        profile.setId(1L);
        return StudentActivity.builder()
                .id(1L)
                .attempts(List.of(new Attempt(1L, 1L, null)))
                .status(ActivityStatus.NOT_STARTED)
                .profile(profile)
                .activity(createActivity())
                .build();
    }

    private Activity createActivity() {
        return Activity.builder()
                .id(1L)
                .isDeleted(false)
                .build();
    }

}
