package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.service.AttemptService;
import com.java.nix.cengage.service.AttemptValidationService;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class AttemptControllerTest {

    private static final String REQUEST_PATH = "/attempt";

    @Mock
    private AttemptService attemptService;

    @Mock
    private AttemptValidationService validationService;

    @Mock
    private StudentActivityService studentActivityService;

    private Attempt attempt;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.attempt = createAttempt();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                new AttemptController(studentActivityService, validationService, attemptService))
                .build();
    }

    @Test
    public void getAllTest() throws Exception {
        doReturn(List.of(attempt)).when(attemptService).getAll();

        mockMvc.perform(get(REQUEST_PATH)).andExpect(status().isOk());
    }

    @Test
    public void getAllByStudentActivityIdTest() throws Exception {
        doReturn(List.of(attempt)).when(attemptService).getAllByStudentActivityId(1L);

        mockMvc.perform(get(REQUEST_PATH + "/" + 1)).andExpect(status().isOk());
    }

    @Test
    public void saveAttemptTest() throws Exception {
        doReturn(attempt).when(attemptService).insert(attempt);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isCreated());

    }

    private Attempt createAttempt() {
        StudentActivity studentActivity = new StudentActivity();
        studentActivity.setId(1L);
        return new Attempt(1L, 1L, studentActivity);
    }

    private String getArticleInJson() {
        return "{\n" + "  \"id\": 1,\n" + "  \"mark\": 1,\n" + "  \"studentActivityId\": 1\n"
                + "}";
    }
}