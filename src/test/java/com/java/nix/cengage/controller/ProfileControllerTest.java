package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.Role;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.entity.dto.ProfileDto;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.ProfileValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.java.nix.cengage.entity.enums.ProfileRole.TEACHER;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ProfileControllerTest {

    @Mock
    private ProfileValidationService validationService;

    @Mock
    private ProfileService profileService;

    private MockMvc mockMvc;

    private Profile profile;

    private Course course;

    private Role role;

    private User user;

    @BeforeEach
    void setup() {
        this.user = createUser();
        this.role = createRole();
        this.course = createCourse();
        this.profile = createProfile();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                new ProfileController(profileService, validationService))
                .build();
    }

    @Test
    public void getAllTest() throws Exception {
        doReturn(List.of(profile)).when(profileService).getAll();

        mockMvc.perform(get("/profile").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getByIdTest() throws Exception {
        doReturn(profile).when(profileService).getById(1L);

        mockMvc.perform(get("/profile/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getByUserIdTest() throws Exception {
        doReturn(List.of(profile)).when(profileService).findProfilesOfGivenUser(1L);

        mockMvc.perform(get("/profile/user/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        lenient().when(profileService.existsById(1L)).thenReturn(true);

        mockMvc.perform(delete("/profile/1")).andExpect(status().isOk());
    }

    @Test
    public void createTest() throws Exception {
        lenient().when(profileService.insertStudent(profileForCreateMethod())).thenReturn(profile);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/profile/student")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isCreated());
    }

    @Test
    public void getAllStudentsTest() throws Exception {
        lenient().when(profileService.existsByRoleIdAndUserIdAndCourseId(TEACHER.getId(), 1L, 1L))
                .thenReturn(true);
        lenient().when(profileService.findStudentsOfGivenCourse(1L)).thenReturn(List.of(profile));

        mockMvc.perform(get("/profile/teacher/1/course/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private Role createRole() {
        String name = "user";
        return new Role(1L, name);
    }

    private Profile profileForCreateMethod() {
        return ProfileDto.toProfile(new ProfileDto(1L, 1L, 1L, 1L));
    }

    private Profile createProfile() {
        return Profile.builder()
                .course(course)
                .user(user)
                .role(role)
                .build();
    }

    private Course createCourse() {
        return Course.builder()
                .id(1L)
                .creator(createUser())
                .isDeleted(false)
                .build();
    }

    private User createUser() {
        return User.builder()
                .id(1L)
                .isDeleted(false)
                .build();
    }

    private String getArticleInJson() {
        return "{\n" + "  \"courseId\": 1,\n" + "  \"id\": 1,\n" + "  \"roleId\": 1,\n"
                + "  \"userId\": 1\n" + "}";
    }
}