package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.service.ActivityService;
import com.java.nix.cengage.service.CourseService;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ActivityControllerTest {

    @Mock
    private StudentActivityService studentActivityService;

    @Mock
    private CourseService courseService;

    @Mock
    private ActivityService activityService;

    @Mock
    private ProfileService profileService;

    private Activity activity;

    private Course course;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.course = createCourse();
        this.activity = createActivity();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                new ActivityController(courseService, activityService))
                .build();
    }

    @Test
    public void getByIdTest() throws Exception {
        doReturn(activity).when(activityService).getById(1L);

        mockMvc.perform(get("/activity/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllTest() throws Exception {
        lenient().when(activityService.getAll()).thenReturn(List.of(activity));

        mockMvc.perform(get("/activity").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void addActivityTest() throws Exception {
        doReturn(course).when(courseService).getById(1L);
        doReturn(activity).when(activityService).insert(activity);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/activity")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isCreated());

        verify(activityService, times(1)).insert(activity);
    }

    @Test
    public void updateTest() throws Exception {
        lenient().when(activityService.existsById(1L)).thenReturn(true);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/activity")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isOk());

        verify(activityService, times(1)).update(activity);

    }

    @Test
    public void deleteByIdTest() throws Exception {

        mockMvc.perform(delete("/activity/1")).andExpect(status().isOk());

        verify(activityService, times(1)).deleteById(1L);
    }

    @Test
    public void getActivitiesByCourseTest() throws Exception {
        doReturn(Set.of(activity)).when(activityService).getActivitiesByCourse(1L);

        mockMvc.perform(get("/activity/course/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    private Activity createActivity() {
        return Activity.builder()
                .id(1L)
                .name("Test")
                .contentLink("link")
                .maxMark(100L)
                .course(createCourse())
                .isDeleted(false)
                .build();
    }

    private Course createCourse() {
        return Course.builder()
                .id(1L)
                .name("Test")
                .isDeleted(false)
                .build();
    }

    private String getArticleInJson() {
        return "{\n" + "  \"contentLink\": \"link\",\n" + "  \"courseId\": 1,\n" + "  \"id\": 1,\n"
                + "  \"name\": \"Test\"\n" + "}";
    }
}