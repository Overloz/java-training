package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.service.CourseService;
import com.java.nix.cengage.service.CourseValidationService;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class CourseControllerTest {

    private static final String REQUEST_PATH = "/course";
    private static final String GET_COURSES_OF_STUDENT_PATH = "/course/user/";

    @Mock
    private UserService userService;

    @Mock
    private CourseService courseService;

    @Mock
    private CourseValidationService courseValidationService;

    @Mock
    private ProfileService profileService;

    private Course course;

    private User user;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.user = createUser();
        this.course = createCourse();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                new CourseController(courseValidationService, profileService, courseService)).build();
    }

    @Test
    void getByIdTest() throws Exception {
        doReturn(course).when(courseService).getById(1L);

        mockMvc.perform(get(REQUEST_PATH + "/" + 1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void getAllTest() throws Exception {
        doReturn(List.of(course)).when(courseService).getAll();

        mockMvc.perform(get(REQUEST_PATH).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void getCoursesOfStudentTest() throws Exception {
        lenient().when(courseService.getAll()).thenReturn(List.of(course));

        mockMvc.perform(get(GET_COURSES_OF_STUDENT_PATH + 1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void updateTest() throws Exception {
        course.setCreator(new User());

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put(REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isOk());

        verify(courseService, times(1)).update(course);
    }

    @Test
    void deleteByIdTest() throws Exception {
        mockMvc.perform(delete(REQUEST_PATH + "/" + 1)).andExpect(status().isOk());

        verify(courseService, times(1)).deleteById(1L);
    }

    @Test
    void createTest() throws Exception {
        lenient().when(courseValidationService.checkIfTeacherAlreadyHasCourseWithGivenName(1L, ""))
                .thenReturn(true);
        lenient().when(userService.getById(1L)).thenReturn(user);
        lenient().when(courseService.insert(course)).thenReturn(course);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isCreated());

    }

    private Course createCourse() {
        String name = "Test";
        return Course.builder().id(1L).name(name).creator(createUser()).isDeleted(false).build();
    }

    private User createUser() {
        return User.builder().id(1L).build();
    }

    private String getArticleInJson() {
        return "{\n" + "  \"name\": \"Test\",\n" + "  \"teacherId\": \"1\"\n" + "}";
    }
}