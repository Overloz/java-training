package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.cache.ActivityContent;
import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.service.ActivityContentService;
import com.java.nix.cengage.service.ActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ActivityContentControllerTest {
    private static final String REQUEST_PATH = "/activitycontent";
    private static final String TEST_STRING = "test";

    @Mock
    private ActivityService activityService;
    @Mock
    private ActivityContentService activityContentService;

    private MockMvc mockMvc;

    private ActivityContent activityContent;

    private Activity activity;

    @BeforeEach
    public void setup() {
        this.activityContent = createActivityContent();
        this.activity = createActivity();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                new ActivityContentController(activityService, activityContentService)).build();
    }

    @Test
    public void getMetaInfoTest() throws Exception {
        doReturn(activity).when(activityService).getById(1L);
        doReturn(activityContent).when(activityContentService).getInfoByLink(TEST_STRING);

        mockMvc.perform(get("/activitycontent/activity/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateTest() throws Exception {

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put(REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isOk());

        verify(activityContentService, times(1)).update(createActivityContent());
    }

    @Test
    public void insertTest() throws Exception {
        doReturn(activityContent).when(activityContentService).insert(activityContent);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isCreated());

        verify(activityContentService, times(1)).insert(createActivityContent());
    }

    private Activity createActivity() {
        return Activity.builder()
                .id(1L)
                .name(TEST_STRING)
                .contentLink(TEST_STRING)
                .isDeleted(false).build();
    }

    private ActivityContent createActivityContent() {
        return new ActivityContent(TEST_STRING, TEST_STRING, TEST_STRING );
    }

    private String getArticleInJson() {
        return "{\n" + "  \"content\": \"test\",\n" + "  \"description\": \"test\",\n"
                + "  \"link\": \"test\"\n" + "}";
    }
}