package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.UserService;
import com.java.nix.cengage.service.UserValidationService;
import com.java.nix.cengage.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private ProfileService profileService;

    @Mock
    private UserValidationService userValidationService;

    private User user;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.user = createUser();
        this.userService = Mockito.mock(UserServiceImpl.class);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(new UserController(userValidationService, userService)).build();
    }

    @Test
    public void getAllTest() throws Exception {
        doReturn(List.of(user)).when(userService).getAll();

        mockMvc.perform(get("/user").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void getByIdTest() throws Exception {
        doReturn(user).when(userService).getById(1L);

        mockMvc.perform(get("/user/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() throws Exception {
        lenient().when(userService.existsById(1L)).thenReturn(true);

        mockMvc.perform(delete("/user/1")).andExpect(status().isOk());
    }

    @Test
    public void createUserTest() throws Exception {
        doReturn(user).when(userService).saveUser(user);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isCreated());
    }

    @Test
    public void update() throws Exception {
        doNothing().when(userService).update(user);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/user")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getArticleInJson());

        mockMvc.perform(builder).andExpect(status().isOk());
    }

    private User createUser() {
        String username = "Test";
        String email = "test@gmail.com";
        return User.builder()
                .id(1L)
                .username(username)
                .email(email)
                .isDeleted(false)
                .build();
    }

    private String getArticleInJson() {
        return "{\n" + "  \"email\": \"test@gmail.com\",\n" + "  \"id\": 1,\n"
                + "  \"username\": \"Test\"\n" + "}";
    }
}