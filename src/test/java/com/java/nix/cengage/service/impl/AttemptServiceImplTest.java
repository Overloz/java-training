package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.entity.enums.ActivityStatus;
import com.java.nix.cengage.repositories.mysql.AttemptRepository;
import com.java.nix.cengage.service.AttemptValidationService;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AttemptServiceImplTest {

    @InjectMocks
    private AttemptServiceImpl attemptService;

    @Mock
    private StudentActivityService studentActivityService;
    @Mock
    private AttemptValidationService validationService;
    @Mock
    private AttemptRepository attemptRepository;

    private Attempt attempt;

    @BeforeEach
    public void setUp() {
        this.attempt = createAttempt();
    }

    @Test
    public void getAllTest() {
        doReturn(List.of(attempt)).when(attemptRepository).findAll();

        attemptService.getAll();

        verify(attemptRepository, times(1)).findAll();
    }

    @Test
    public void getByIdTest() {
        doReturn(Optional.of(attempt)).when(attemptRepository).findById(1L);

        attemptService.getById(1L);

        verify(attemptRepository, times(1)).findById(1L);
    }

    @Test
    public void getAllByStudentActivityIdTest() {
        doReturn(List.of(attempt)).when(attemptRepository).findAllByStudentActivityId(1L);

        attemptService.getAllByStudentActivityId(1L);

        verify(attemptRepository, times(1)).findAllByStudentActivityId(1L);
    }

    @Test
    public void insertTest() {
        lenient().when(attemptRepository.save(attempt)).thenReturn(attempt);
        lenient().when(studentActivityService.getById(1L)).thenReturn(createStudentActivity());

        Attempt saved = attemptService.insert(attempt);

        verify(validationService, times(1)).checkIfMarkIsNotNegativeOrNotBiggerThanMax(attempt);
        verify(attemptRepository, times(1)).save(attempt);

        assertEquals(saved, attempt);
    }

    private Attempt createAttempt() {
        Long maxMark = 100L;
        StudentActivity studentActivity = new StudentActivity();
        studentActivity.setId(1L);
        return new Attempt(1L, maxMark, studentActivity);
    }

    private StudentActivity createStudentActivity() {
        Profile profile = new Profile();
        profile.setId(1L);
        return StudentActivity.builder().id(1L).status(ActivityStatus.NOT_STARTED)
                .activity(createActivity()).build();
    }

    private Activity createActivity() {
        return Activity.builder().id(1L).maxMark(100L).isDeleted(false).build();
    }
}