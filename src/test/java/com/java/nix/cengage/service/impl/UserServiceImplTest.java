package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.repositories.mysql.UserRepository;
import com.java.nix.cengage.service.ProfileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final String TEST_USERNAME = "Test";
    private static final String TEST_EMAIL = "test@gmail.com";

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private ProfileService profileService;

    @Mock
    private UserRepository userRepository;

    private User user;

    @BeforeEach
    public void setUp() {
        this.user = createUser();
    }

    @Test
    public void getByIdTest() {
        lenient().when(userRepository.getById(1L)).thenReturn(user);

        userService.getById(1L);

        verify(userRepository, times(1)).findById(1L);
    }

    @Test
    public void getAllTest() {
        lenient().when(userRepository.findAll()).thenReturn(List.of(user));

        userService.getAll();

        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void saveUserTest() {
        lenient().when(userRepository.save(user)).thenReturn(user);

        userService.saveUser(user);

        verify(userRepository,times(1)).save(user);
    }

    @Test
    public void updateTest() {
        lenient().when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        userService.update(user);

        verify(userRepository,times(1)).save(user);
    }

    @Test
    public void deleteTest() {
        lenient().when(userRepository.existsById(1L)).thenReturn(true);

        userService.deleteById(1L);

        verify(userRepository,times(1)).deleteUserById(1L);
    }

    @Test
    public void existByUsernameTest(){
        lenient().when(userRepository.existsByUsername(TEST_USERNAME)).thenReturn(true);

        userService.existsByUsername(TEST_USERNAME);

        verify(userRepository, times(1)).existsByUsername(TEST_USERNAME);
    }

    @Test
    public void existByEmailTest(){
        lenient().when(userRepository.existsByEmail(TEST_EMAIL)).thenReturn(true);

        userService.existsByEmail(TEST_EMAIL);

        verify(userRepository, times(1)).existsByEmail(TEST_EMAIL);
    }

    @Test
    public void existsByIdTest(){
        lenient().when(userRepository.existsById(1L)).thenReturn(true);

        userService.existsById(1L);

        verify(userRepository,times(1)).existsById(1L);
    }

    private User createUser() {
        return User.builder()
                .id(1L)
                .username(TEST_USERNAME)
                .email(TEST_EMAIL)
                .isDeleted(false)
                .build();
    }
}