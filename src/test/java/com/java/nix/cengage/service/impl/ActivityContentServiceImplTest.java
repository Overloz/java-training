package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.cache.ActivityContent;
import com.java.nix.cengage.repositories.mongo.ActivityContentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ActivityContentServiceImplTest {

    private static final String TEST_STRING = "test";

    @InjectMocks
    private ActivityContentServiceImpl activityContentService;

    @Mock
    private ActivityContentRepository activityContentRepository;

    private ActivityContent activityContent;

    private Activity activity;

    @BeforeEach
    public void setUp() {
        this.activity = createActivity();
        this.activityContent = createActivityContent();
    }

    @Test
    public void getInfoByActivityTest() {
        doReturn(Optional.of(activityContent)).when(activityContentRepository)
                .findByLink(TEST_STRING);

        ActivityContent contentInDatabase = activityContentService
                .getInfoByLink(activity.getContentLink());

        verify(activityContentRepository, times(1)).findByLink(TEST_STRING);

        assertEquals(activityContent,contentInDatabase);
    }

    @Test
    public void insertTest() {
        lenient().when(activityContentRepository.insert(activityContent))
                .thenReturn(activityContent);

        activityContentService.insert(activityContent);

        verify(activityContentRepository, times(1)).save(activityContent);
    }

    @Test
    public void updateTest() {
        lenient().when(activityContentRepository.findByLink(TEST_STRING))
                .thenReturn(Optional.ofNullable(activityContent));

        activityContentService.update(activityContent);

        verify(activityContentRepository, times(1)).save(activityContent);
    }

    private ActivityContent createActivityContent() {
        return new ActivityContent(TEST_STRING, TEST_STRING, TEST_STRING);
    }

    private Activity createActivity() {
        return Activity.builder().id(1L).contentLink(TEST_STRING).isDeleted(false).build();
    }
}