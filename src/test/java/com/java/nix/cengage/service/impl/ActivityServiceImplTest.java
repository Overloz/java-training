package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.*;
import com.java.nix.cengage.entity.cache.Cache;
import com.java.nix.cengage.exception.search.ActivityNotFoundException;
import com.java.nix.cengage.repositories.mysql.ActivityRepository;
import com.java.nix.cengage.service.CacheService;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ActivityServiceImplTest {

    @InjectMocks
    private ActivityServiceImpl activityService;

    @Mock
    private ProfileService profileService;

    @Mock
    private CacheService cacheService;

    @Mock
    private StudentActivityService studentActivityService;

    @Mock
    private ActivityRepository activityRepository;

    private Activity activity;

    private Cache cache;

    @BeforeEach
    public void setup() {
        this.cache = createCourseActivity();
        this.activity = createActivity();
    }

    @Test
    public void getAllTest() {
        doReturn(List.of(activity)).when(activityRepository).findAll();

        activityService.getAll();

        verify(activityRepository, times(1)).findAll();
    }

    @Test
    public void deleteAllByCourseId() {
        activityService.deleteAllByCourseId(1L);

        verify(activityRepository, times(1)).deleteAllByCourseId(1L);
    }

    @Test
    public void insertTest() {
        lenient().when(activityRepository.save(activity)).thenReturn(activity);
        lenient().when(profileService.findStudentsOfGivenCourse(1L)).thenReturn(List.of(createProfile()));

        activityService.insert(activity);

        verify(activityRepository, times(1)).save(activity);
    }

    @Test
    public void getByIdTest() {
        doReturn(Optional.of(activity)).when(activityRepository).findById(1L);

        activityService.getById(1L);

        verify(activityRepository, times(1)).findById(1L);
    }

    @Test
    public void updateTest() {
        doReturn(Optional.of(activity)).when(activityRepository).findById(1L);

        activityService.update(activity);

        verify(activityRepository, times(1)).save(activity);
    }

    @Test
    public void existsByIdTest() {
        lenient().when(activityRepository.existsById(1L)).thenReturn(true);

        activityService.existsById(1L);

        verify(activityRepository, times(1)).existsById(1L);
    }

    @Test
    public void updateWithExceptionTest() {
        lenient().when(activityRepository.getById(1L)).thenThrow(new ActivityNotFoundException(1L));

        Assertions.assertThrows(ActivityNotFoundException.class,
                () -> activityService.update(activity));
    }

    @Test
    public void deleteTest() {
        doReturn(Optional.of(activity)).when(activityRepository).findById(1L);

        activityService.deleteById(1L);

        verify(activityRepository, times(1)).deleteActivityById(1L);
    }

    @Test
    public void deleteWithExceptionTest() {
        lenient().when(activityRepository.getById(1L)).thenThrow(new ActivityNotFoundException(1L));

        Assertions.assertThrows(ActivityNotFoundException.class,
                () -> activityService.deleteById(1L));
    }

    @Test
    public void getActivitiesByCourseFromMySqlTest() {
        doReturn(Optional.of(cache)).when(cacheService).find(1L);

        activityService.getActivitiesByCourse(1L);

        verify(cacheService, times(1)).find(1L);
    }

    @Test
    public void getActivitiesByCourseFromCacheTest() {
        activityService.getActivitiesByCourse(1L);

        verify(activityRepository, times(1)).findAllByCourseId(1L);
        verify(cacheService, times(1)).insert(new HashSet<>(), 1L);
    }

    private Activity createActivity() {
        return Activity.builder()
                .id(1L)
                .maxMark(100L)
                .course(createCourse())
                .isDeleted(false)
                .build();
    }

    private Role createRole() {
        String name = "user";
        return new Role(1L, name);
    }

    private Profile createProfile() {
        return Profile.builder()
                .id(1L)
                .course(createCourse())
                .user(createUser())
                .role(createRole())
                .build();
    }

    private Course createCourse() {
        return Course.builder()
                .id(1L)
                .creator(createUser())
                .isDeleted(false)
                .build();
    }

    private User createUser() {
        return User.builder()
                .id(1L)
                .isDeleted(false)
                .build();
    }

    private Cache createCourseActivity() {
        return new Cache(1L, new HashSet<>());
    }
}