package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.cache.Cache;
import com.java.nix.cengage.repositories.mongo.CacheRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CacheServiceImplTest {

    @InjectMocks
    private CacheServiceImpl courseActivityService;

    @Mock
    private CacheRepository cacheRepository;

    private Cache cache;

    @BeforeEach
    public void setUp() {
        this.cache = createCourseActivity();
    }

    @Test
    public void delete() {
        lenient().when(cacheRepository.findById(1L)).thenReturn(Optional.of(cache));

        courseActivityService.delete(1L);

        verify(cacheRepository, times(1)).deleteById(1L);
    }

    @Test
    public void insert() {
        courseActivityService.insert(new HashSet<>(), 1L);

        verify(cacheRepository, times(1)).insert(cache);
    }

    @Test
    public void find() {
        doReturn(Optional.of(cache)).when(cacheRepository).findById(1L);

        courseActivityService.find(1L);

        verify(cacheRepository, times(1)).findById(1L);
    }

    private Cache createCourseActivity() {
        return new Cache(1L, new HashSet<>());
    }
}