package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Role;
import com.java.nix.cengage.repositories.mysql.RoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTest {

    @InjectMocks
    private RoleServiceImpl roleService;

    @Mock
    private RoleRepository roleRepository;

    private Role role;

    @BeforeEach
    public void setUp(){
        this.role = createRole();
    }

    @Test
    public void insertTest() {
        lenient().when(roleRepository.save(role)).thenReturn(role);

        roleService.insert(role);

        verify(roleRepository,times(1)).save(role);
    }

    @Test
    public void findByIdTest() {
        lenient().when(roleRepository.findById(1L)).thenReturn(Optional.of(role));

        roleService.findById(1L);

        verify(roleRepository, times(1)).findById(1L);
    }

    private Role createRole() {
        String roleName = "test";
        return new Role(1L, roleName);
    }
}