package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.*;
import com.java.nix.cengage.entity.enums.ActivityStatus;
import com.java.nix.cengage.entity.enums.ProfileState;
import com.java.nix.cengage.repositories.mysql.StudentActivityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static com.java.nix.cengage.entity.enums.ActivityStatus.FINALIZED;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentActivityServiceImplTest {

    private static final String TEST_STRING = "TEST";

    @InjectMocks
    private StudentActivityServiceImpl studentActivityService;
    @Mock
    private StudentActivityRepository studentActivityRepository;

    private StudentActivity studentActivity;

    private Profile profile;

    private Activity activity;

    @BeforeEach
    public void setUp() {
        this.profile = createProfile();
        this.studentActivity = createStudentActivity();
        this.activity = createActivity();
    }

    @Test
    public void getResultsTest() {
        lenient().when(studentActivityRepository.findAllByProfileIdAndActivityCourseId(1L, 1L))
                .thenReturn(List.of(studentActivity));

        studentActivityService.getResults(1L, 1L);

        verify(studentActivityRepository, times(1)).findAllByProfileIdAndActivityCourseId(1L, 1L);
    }

    @Test
    public void addActivitiesToStudentTest() {
        studentActivityService.addActivitiesToStudent(profile);

        StudentActivity studentActivity = StudentActivity.studentActivityBuilder(activity, profile);

        verify(studentActivityRepository, times(1)).save(studentActivity);
    }

    @Test
    public void updateStatusByActivityIdAndProfileId() {
        studentActivityService.updateStatusByActivityIdAndProfileId(FINALIZED, 1L, 1L);

        verify(studentActivityRepository, times(1))
                .updateStatusByActivityIdProfileId(FINALIZED.getName(), 1L, 1L);
    }

    @Test
    public void addActivityToStudentsTest() {
        studentActivityService.addActivityToStudents(activity, List.of(profile));

        verify(studentActivityRepository, times(1)).save(studentActivity);

    }

    private Activity createActivity() {
        long maxMark = 100L;
        return Activity.builder().id(1L).name(TEST_STRING).contentLink(TEST_STRING).maxMark(maxMark).course(null)
                .isDeleted(false).build();
    }

    private StudentActivity createStudentActivity() {
        return StudentActivity.builder().status(ActivityStatus.NOT_STARTED).profile(createProfile())
                .activity(createActivity()).build();
    }

    private Profile createProfile() {
        return Profile.builder().id(1L).course(createCourse()).user(createUser())
                .state(ProfileState.ACTIVE).build();
    }

    private Course createCourse() {
        return Course.builder().id(1L).name(TEST_STRING).creator(createUser())
                .activities(Set.of(createActivity())).isDeleted(false).build();
    }

    private User createUser() {
        return User.builder().id(1L).username(TEST_STRING).email(TEST_STRING).isDeleted(false)
                .build();
    }

}