package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.Role;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.exception.search.ProfileNotFoundException;
import com.java.nix.cengage.repositories.mysql.ProfileRepository;
import com.java.nix.cengage.service.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProfileServiceImplTest {

    private static final String TEST_NAME = "Test";
    private static final String TEST_EMAIL = "test@gmail.com";

    @InjectMocks
    private ProfileServiceImpl profileService;
    @Mock
    private StudentActivityService studentActivityService;
    @Mock
    private UserService userService;
    @Mock
    private CourseService courseService;
    @Mock
    private RoleService roleService;
    @Mock
    private ProfileRepository profileRepository;

    private Profile profile;

    @BeforeEach
    public void setUp() {
        this.profile = createProfile();
    }

    @Test
    public void findStudentsOfGivenCourseTest() {
        lenient().when(profileRepository.findAllStudentsByCourseId(1L))
                .thenReturn(List.of(profile));

        profileService.findStudentsOfGivenCourse(1L);

        verify(profileRepository, times(1)).findAllStudentsByCourseId(1L);
    }

    @Test
    public void findProfilesOfGivenUserTest() {
        lenient().when(profileRepository.findAllByUserId(1L)).thenReturn(List.of(profile));

        profileService.findProfilesOfGivenUser(1L);

        verify(profileRepository, times(1)).findAllByUserId(1L);
    }

    @Test
    public void getAllTestTest() {
        lenient().when(profileRepository.findAll()).thenReturn(List.of(profile));

        profileService.getAll();

        verify(profileRepository, times(1)).findAll();
    }

    @Test
    public void insertTest() {
        lenient().when(userService.getById(1L)).thenReturn(createUser());
        lenient().when(courseService.getById(1L)).thenReturn(createCourse());
        lenient().when(roleService.findById(1L)).thenReturn(createRole());
        lenient().when(profileRepository.save(profile)).thenReturn(profile);

        profileService.insert(profile);

        verify(profileRepository, times(1)).save(profile);
    }

    @Test
    public void insertStudentTest() {
        lenient().when(userService.getById(1L)).thenReturn(createUser());
        lenient().when(courseService.getById(1L)).thenReturn(createCourse());
        lenient().when(roleService.findById(1L)).thenReturn(createRole());
        lenient().when(profileRepository.save(profile)).thenReturn(profile);

        Profile saved = profileService.insertStudent(profile);

        verify(profileRepository, times(1)).save(saved);
    }

    @Test
    public void getByIdTest() {
        lenient().when(profileRepository.getById(1L)).thenReturn(profile);

        profileService.getById(1L);

        verify(profileRepository, times(1)).findById(1L);
    }

    @Test
    public void update() {
        profileService.update(profile);

        verify(profileRepository, times(1)).save(profile);
    }

    @Test
    public void deleteByIdTest() {
        lenient().when(profileRepository.existsById(1L)).thenReturn(true);

        profileService.deleteById(1L);

        verify(profileRepository).deleteProfileById(1L);
    }

    @Test
    public void deleteByIdWithExceptionTest() {
        lenient().when(profileRepository.existsById(1L)).thenReturn(false);

        Assertions.assertThrows(ProfileNotFoundException.class, () -> profileService.deleteById(1L));
    }

    @Test
    public void existsByIdTest() {
        lenient().when(profileRepository.existsById(1L)).thenReturn(true);

        profileService.existsById(1L);

        verify(profileRepository, times(1)).existsById(1L);
    }

    @Test
    public void existsByRoleIdAndUserIdAndCourseIdTest() {
        lenient().when(profileRepository.existsByUserIdAndRoleIdAndCourseId(1L, 1L, 1L))
                .thenReturn(true);

        profileService.existsByRoleIdAndUserIdAndCourseId(1L, 1L, 1L);

        verify(profileRepository, times(1)).existsByUserIdAndRoleIdAndCourseId(1L, 1L, 1L);
    }

    private Role createRole() {
        return new Role(1L, TEST_NAME);
    }

    private Profile createProfile() {
        return Profile.builder()
                .id(1L)
                .course(createCourse())
                .user(createUser())
                .role(createRole())
                .build();
    }

    private Course createCourse() {
        return Course.builder()
                .id(1L)
                .name(TEST_NAME)
                .creator(createUser())
                .isDeleted(false)
                .build();
    }

    private User createUser() {
        return User.builder()
                .id(1L)
                .username(TEST_NAME)
                .email(TEST_EMAIL)
                .isDeleted(false)
                .build();
    }

}