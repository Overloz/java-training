package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.exception.validation.ProfileValidationException;
import com.java.nix.cengage.service.ProfileService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.java.nix.cengage.entity.enums.ProfileRole.STUDENT;
import static com.java.nix.cengage.entity.enums.ProfileRole.TEACHER;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProfileValidationServiceImplTest {

    @InjectMocks
    private ProfileValidationServiceImpl profileValidationService;

    @Mock
    private ProfileService profileService;

    @Test
    public void validateTeacherAndCourseTest() {
        lenient().when(profileService.existsByRoleIdAndUserIdAndCourseId(TEACHER.getId(), 1L, 1L))
                .thenReturn(true);

        profileValidationService.checkIfUserIsTeacherOnGivenCourse(1L, 1L);

        verify(profileService, times(1))
                .existsByRoleIdAndUserIdAndCourseId(TEACHER.getId(), 1L, 1L);
    }

    @Test
    public void validateTeacherAndCourseWithExceptionTest() {
        lenient().when(profileService.existsByRoleIdAndUserIdAndCourseId(STUDENT.getId(), 1L, 1L))
                .thenReturn(false);

        Assertions.assertThrows(ProfileValidationException.class,
                () -> profileValidationService.checkIfUserIsTeacherOnGivenCourse(1L, 1L));

    }

}