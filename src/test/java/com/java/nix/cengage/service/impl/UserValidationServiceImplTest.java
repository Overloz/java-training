package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.exception.validation.UserValidationException;
import com.java.nix.cengage.service.UserService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserValidationServiceImplTest {

    private static final String TEST_EMAIL = "test@gmail.com";
    private static final String TEST_USERNAME = "Test";

    @InjectMocks
    private UserValidationServiceImpl validationService;

    @Mock
    private UserService userService;

    @Test
    public void checkForExistingUserWithExceptionWhenSameUsernameExistsTest() {
        lenient().when(userService.existsByEmail(TEST_EMAIL)).thenReturn(false);
        lenient().when(userService.existsByUsername(TEST_USERNAME)).thenReturn(true);

        Assertions.assertThrows(UserValidationException.class,
                () -> validationService.checkThatUserNotExists(createUser()));

        verify(userService, times(1)).existsByUsername(TEST_USERNAME);
    }

    private User createUser() {
        return User.builder()
                .id(1L)
                .username(TEST_USERNAME)
                .email(TEST_EMAIL)
                .isDeleted(false)
                .build();
    }

    @Test
    public void test(){
        for(int i = 0; i < 100; i++){
            System.out.println(RandomString.make(64).toUpperCase());
        }
    }
}
