package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.exception.search.CourseNotFoundException;
import com.java.nix.cengage.repositories.mysql.CourseRepository;
import com.java.nix.cengage.service.ActivityService;
import com.java.nix.cengage.service.CacheService;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

    private static final String TEST_NAME = "Test";

    @InjectMocks
    private CourseServiceImpl courseService;

    @Mock
    private ProfileService profileService;

    @Mock
    private ActivityService activityService;

    @Mock
    private CourseRepository courseRepository;

    @Mock
    private UserService userService;

    @Mock
    private CacheService cacheService;

    private Course course;

    @BeforeEach
    public void setUp() {
        this.course = createCourse();
    }

    @Test
    public void getAllTest() {
        doReturn(List.of(course)).when(courseRepository).findAll();

        courseService.getAll();

        verify(courseRepository, times(1)).findAll();
    }

    @Test
    public void insertTest() {
        lenient().when(userService.getById(1L)).thenReturn(createUser());
        doReturn(course).when(courseRepository).save(course);

        courseService.insert(course);

        verify(courseRepository, times(1)).save(course);

    }

    @Test
    public void findAllCoursesForStudentTest() {
        doReturn(List.of(course)).when(courseRepository).findAllByStudentsId(1L);

        courseService.findAllCoursesForStudent(1L);

        verify(courseRepository, times(1)).findAllByStudentsId(1L);
    }

    @Test
    public void getByIdTest() {
        doReturn(Optional.of(course)).when(courseRepository).findById(1L);

        courseService.getById(1L);

        verify(courseRepository, times(1)).findById(1L);
    }

    @Test
    public void getByIdWithExceptionTest() {
        lenient().when(courseRepository.getById(1L)).thenThrow(new CourseNotFoundException(1L));

        Assertions.assertThrows(CourseNotFoundException.class, () -> courseRepository.getById(1L));
    }

    @Test
    public void updateTest() {
        doReturn(Optional.of(course)).when(courseRepository).findById(1L);

        courseService.update(course);

        verify(courseRepository, times(1)).save(course);
    }

    @Test
    public void updateWithExceptionTest() {
        lenient().when(courseRepository.getById(1L)).thenThrow(new CourseNotFoundException(1L));

        Assertions.assertThrows(CourseNotFoundException.class, () -> courseService.update(course));
    }

    @Test
    public void deleteTest() {
        lenient().when(courseRepository.existsById(1L)).thenReturn(true);
        courseService.deleteById(1L);

        verify(courseRepository, times(1)).deleteCourseById(1L);
        verify(activityService, times(1)).deleteAllByCourseId(1L);
        verify(cacheService, times(1)).delete(1L);
    }

    @Test
    public void deleteWithExceptionTest() {
        lenient().when(courseRepository.existsById(1L)).thenReturn(false);

        Assertions.assertThrows(CourseNotFoundException.class, () -> courseService.deleteById(1L));
    }

    @Test
    public void existsByIdTest() {
        lenient().when(courseRepository.existsById(1L)).thenReturn(true);

        courseService.existsById(1L);

        verify(courseRepository, times(1)).existsById(1L);
    }

    private Course createCourse() {
        return Course.builder()
                .id(1L)
                .name(TEST_NAME)
                .creator(createUser())
                .isDeleted(false)
                .build();
    }

    private User createUser() {
        String email = "test@gmail.com";
        return User.builder()
                .id(1L)
                .username(TEST_NAME)
                .email(email)
                .isDeleted(false)
                .build();
    }
}