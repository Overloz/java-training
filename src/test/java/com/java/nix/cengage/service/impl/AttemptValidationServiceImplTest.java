package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.exception.validation.AttemptValidationException;
import com.java.nix.cengage.service.StudentActivityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AttemptValidationServiceImplTest {

    @InjectMocks
    private AttemptValidationServiceImpl attemptValidationService;

    @Mock
    private StudentActivityService studentActivityService;

    private Attempt attempt;

    @BeforeEach
    public void setUp(){
        attempt = createAttempt();
    }

    @Test
    public void validateWithExceptionOfMarkBiggerThanMaxPossibleTest() {
        Assertions.assertThrows(AttemptValidationException.class,
                () -> attemptValidationService.checkIfMarkIsNotNegativeOrNotBiggerThanMax(attempt));
    }

    @Test
    public void validateWithExceptionOfLessThanZeroTest() {
        attempt.setMark(-100L);
        Assertions.assertThrows(AttemptValidationException.class,
                () -> attemptValidationService.checkIfMarkIsNotNegativeOrNotBiggerThanMax(attempt));
    }

    private Activity createActivity() {
        String name = "Test";
        return Activity.builder()
                .id(1L)
                .name(name)
                .maxMark(100L)
                .isDeleted(false)
                .build();
    }

    private Attempt createAttempt() {
        StudentActivity studentActivity = new StudentActivity();
        studentActivity.setId(1L);
        studentActivity.setActivity(createActivity());
        return new Attempt(1L, 110L, studentActivity);
    }

}
