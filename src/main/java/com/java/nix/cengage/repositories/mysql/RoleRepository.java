package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
