package com.java.nix.cengage.repositories.mongo;

import com.java.nix.cengage.entity.cache.ActivityContent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivityContentRepository extends MongoRepository<ActivityContent, String> {
    Optional<ActivityContent> findByLink(String s);
}
