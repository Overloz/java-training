package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {
    Set<Activity> findAllByCourseId(Long courseId);

    @Modifying
    @Query(value = "UPDATE activity SET is_deleted = true WHERE activity.id = ?1", nativeQuery = true)
    void deleteActivityById(Long id);

    @Modifying
    @Query(value = "UPDATE activity SET is_deleted = true WHERE activity.course_id = ?1", nativeQuery = true)
    void deleteAllByCourseId(Long id);
}