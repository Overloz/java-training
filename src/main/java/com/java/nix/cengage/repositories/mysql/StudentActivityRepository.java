package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.StudentActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentActivityRepository extends JpaRepository<StudentActivity, Long> {

    List<StudentActivity> findAllByProfileIdAndActivityCourseId(Long user, Long course);

    @Modifying
    @Query(value = "UPDATE student_activity SET status=?1 WHERE activity_id = ?2 AND profile_id=?3", nativeQuery = true)
    void updateStatusByActivityIdProfileId(String status, Long activityId, Long profileId);

    List<StudentActivity> findAllByActivityId(Long activityId);

    @Modifying
    @Query(value = "UPDATE student_activity SET status=?1 WHERE activity_id = ?2 ", nativeQuery = true)
    void finalizeAllByActivityId(String status, Long activityId);
}