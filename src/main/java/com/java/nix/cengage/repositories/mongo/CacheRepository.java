package com.java.nix.cengage.repositories.mongo;

import com.java.nix.cengage.entity.cache.Cache;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CacheRepository extends MongoRepository<Cache, Long> {
    void deleteById(Long id);
}
