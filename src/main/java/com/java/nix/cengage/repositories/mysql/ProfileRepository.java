package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {

    boolean existsByUserIdAndRoleIdAndCourseId(Long userId, Long roleId, Long courseId);

    @Modifying
    @Query(value = "SELECT * FROM profile RIGHT JOIN course c on profile.course_id = c.id WHERE c.id = ?1 AND profile.role_id = 1", nativeQuery = true)
    List<Profile> findAllStudentsByCourseId(Long courseId);

    List<Profile> findAllByUserId(Long userId);

    @Modifying
    @Query(value = "UPDATE profile SET profile.state = 'DELETED' WHERE profile.id = ?1", nativeQuery = true)
    void deleteProfileById(Long id);

    @Modifying
    @Query(value = "UPDATE profile SET profile.state = 'DELETED' WHERE profile.user_id = ?1", nativeQuery = true)
    void deleteAllByUserId(Long id);

    @Modifying
    @Query(value = "UPDATE profile SET profile.state = 'DELETED' WHERE profile.course_id = ?1", nativeQuery = true)
    void deleteAllByCourseId(Long id);
}
