package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    @Modifying
    @Transactional
    @Query(value = "UPDATE user SET is_deleted = true WHERE user.id = ?1", nativeQuery = true)
    void deleteUserById(Long id);
}
