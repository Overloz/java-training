package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    @Modifying
    @Query(value = "UPDATE course SET is_deleted = true WHERE course.id = ?1", nativeQuery = true)
    void deleteCourseById(Long id);

    @Modifying
    @Query(value = "SELECT * FROM course RIGHT JOIN profile p on course.id = p.course_id "
            + "RIGHT JOIN user u on u.id = p.user_id WHERE p.user_id = ?1 and p.role_id = 1", nativeQuery = true)
    List<Course> findAllByStudentsId(Long id);

    boolean existsByNameAndCreatorId(String name, Long id);

    Course findByExternalId(String externalId);
}
