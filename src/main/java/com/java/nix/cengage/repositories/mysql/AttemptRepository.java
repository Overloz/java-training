package com.java.nix.cengage.repositories.mysql;

import com.java.nix.cengage.entity.Attempt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttemptRepository extends JpaRepository<Attempt, Long> {
    List<Attempt> findAllByStudentActivityId(Long id);
}
