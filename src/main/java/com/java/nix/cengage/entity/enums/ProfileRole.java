package com.java.nix.cengage.entity.enums;

import com.java.nix.cengage.entity.Role;

public enum ProfileRole {
    TEACHER("teacher", 2L), STUDENT("student", 1L);

    private final String name;

    private final Long id;

    ProfileRole(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Role getRole() {return new Role(id,name);}

    public Long getId() {
        return id;
    }
}
