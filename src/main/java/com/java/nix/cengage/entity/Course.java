package com.java.nix.cengage.entity;

import com.java.nix.cengage.entity.mapped.Recordable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Log4j2
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = { "name", "creator" })
@ToString(of = {"id","name","creator"})
@Where(clause = "is_deleted = false")
@Table(name = "course", uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "creator_id" }) })
public class Course extends Recordable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long isbn;

    @Column(nullable = false, unique = true)
    private String externalId;

    @Column(length = 64, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @Column(name = "start_date")
    private LocalDateTime startOfCourse;

    @Column(name = "end_date")
    private LocalDateTime endingOfCourse;

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private Set<Activity> activities;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @PrePersist
    public void insertCourse() {
        this.isDeleted = false;
    }

}
