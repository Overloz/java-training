package com.java.nix.cengage.entity;

import com.java.nix.cengage.entity.mapped.Recordable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Log4j2
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = {"name","contentLink"})
@Table(name = "activity")
@Where(clause = "is_deleted = false")
public class Activity extends Recordable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, nullable = false)
    private String name;

    @Column(name = "content_link", nullable = false)
    private String contentLink;

    @Column(name = "max_mark", nullable = false)
    private Long maxMark;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @PrePersist
    public void prePersist() {
        this.isDeleted = false;
    }

}
