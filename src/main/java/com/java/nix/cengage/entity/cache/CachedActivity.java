package com.java.nix.cengage.entity.cache;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Course;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CachedActivity {

    private Long id;

    private String name;

    private String content;

    private Long courseId;

    public static CachedActivity fromActivity(Activity activity) {
        return CachedActivity.builder()
                .id(activity.getId())
                .name(activity.getName())
                .content(activity.getContentLink())
                .courseId(activity.getCourse().getId())
                .build();
    }

    public static Activity fromCachedActivity(CachedActivity cachedActivity) {
        Course course = new Course();
        course.setId(cachedActivity.getCourseId());

        Activity activity = new Activity();
        activity.setId(cachedActivity.getId());
        activity.setName(cachedActivity.getName());
        activity.setContentLink(cachedActivity.getContent());
        activity.setCourse(course);
        return activity;
    }
}
