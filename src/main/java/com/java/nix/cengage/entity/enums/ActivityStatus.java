package com.java.nix.cengage.entity.enums;

public enum ActivityStatus {
    NOT_STARTED("NOT_STARTED"), COMPLETED("COMPLETED"), IN_PROGRESS("IN_PROGRESS"),
    FINALIZED("FINALIZED");

    private final String name;

    ActivityStatus(String status) {
        this.name = status;
    }

    public String getName() {
        return name;
    }
}
