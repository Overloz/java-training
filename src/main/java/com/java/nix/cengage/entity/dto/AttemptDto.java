package com.java.nix.cengage.entity.dto;

import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.StudentActivity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class AttemptDto {
    @EqualsAndHashCode.Exclude
    private Long id;

    private Long Mark;

    private Long studentActivityId;

    public static AttemptDto fromAttempt(Attempt attempt) {
        return new AttemptDto(attempt.getId(), attempt.getMark(),
                attempt.getStudentActivity().getId());
    }

    public static Attempt toAttempt(AttemptDto attemptDto) {
        StudentActivity studentActivity = new StudentActivity();
        studentActivity.setId(attemptDto.getId());
        return new Attempt(attemptDto.getId(), attemptDto.getMark(), studentActivity);
    }

}
