package com.java.nix.cengage.entity.cache;

import com.java.nix.cengage.entity.Activity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Document
public class Cache {

    @Id
    private Long id;

    private Set<CachedActivity> activitySet;

    public Cache(Long courseId, Set<Activity> activities) {
        this.id = courseId;
        this.activitySet = activities.stream()
                .map(CachedActivity::fromActivity)
                .collect(Collectors.toSet());
    }
}
