package com.java.nix.cengage.entity.enums;

public enum ProfileState {
    BANNED("banned"), ACTIVE("active"), DELETED("deleted");

    private final String state;

    ProfileState(String state) {
        this.state = state;
    }

    public String getRole() {
        return state;
    }

}
