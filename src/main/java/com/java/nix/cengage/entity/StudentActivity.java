package com.java.nix.cengage.entity;

import com.java.nix.cengage.entity.enums.ActivityStatus;
import com.java.nix.cengage.entity.mapped.Recordable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

import static com.java.nix.cengage.entity.enums.ActivityStatus.NOT_STARTED;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = {"profile","activity"})
@Table(name = "student_activity")
public class StudentActivity extends Recordable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinColumn(name = "user_activity_id", referencedColumnName = "ID")
    private List<Attempt> attempts;

    @Column
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    @Column(name = "final_mark")
    private Long finalMark;

    @ToString.Exclude
    @ManyToOne
    private Profile profile;

    @ToString.Exclude
    @ManyToOne
    private Activity activity;

    public static StudentActivity studentActivityBuilder(Activity activity, Profile student) {
        return StudentActivity.builder()
                .activity(activity)
                .profile(student)
                .status(NOT_STARTED)
                .build();
    }
}
