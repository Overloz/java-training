package com.java.nix.cengage.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(of = {"mark","studentActivity"})
@AllArgsConstructor
public class Attempt {
    @Id
    private Long id;

    @Column(nullable = false)
    private Long mark;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_activity_id")
    private StudentActivity studentActivity;

}
