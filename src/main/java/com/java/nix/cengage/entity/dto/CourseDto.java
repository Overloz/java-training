package com.java.nix.cengage.entity.dto;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
public class CourseDto {

    @EqualsAndHashCode.Exclude
    private Long id;

    @EqualsAndHashCode.Exclude
    private String externalId;

    private Long isbn;

    private String name;

    private Long teacherId;

    private LocalDateTime endingDate;

    private LocalDateTime startingDate;

    public static Course toCourse(CourseDto courseDto) {
        User user = User.builder().id(courseDto.getTeacherId()).build();
        return Course.builder()
                .isbn(courseDto.getIsbn())
                .name(courseDto.getName())
                .endingOfCourse(courseDto.endingDate)
                .startOfCourse(courseDto.startingDate)
                .creator(user).build();
    }

    public static CourseDto fromCourse(Course course) {
        if (course == null) {
            return null;
        }
        return CourseDto.builder()
                .id(course.getId())
                .isbn(course.getIsbn())
                .externalId(course.getExternalId())
                .name(course.getName())
                .teacherId(course.getCreator().getId())
                .startingDate(course.getStartOfCourse())
                .endingDate(course.getEndingOfCourse())
                .build();

    }

}
