package com.java.nix.cengage.entity.dto;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Course;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActivityDto {

    @EqualsAndHashCode.Exclude
    private Long id;

    private String name;

    private String contentLink;

    private Long maxMark;

    private Long courseId;

    public static ActivityDto fromActivity(Activity activity) {
        return ActivityDto.builder()
                .id(activity.getId())
                .name(activity.getName())
                .contentLink(activity.getContentLink())
                .maxMark(activity.getMaxMark())
                .courseId(activity.getCourse().getId())
                .build();
    }

    public static Activity toActivity(ActivityDto activityRequest) {
        Course course = new Course();
        course.setId(activityRequest.getCourseId());
        Activity activity = new Activity();
        activity.setId(activityRequest.getId());
        activity.setName(activityRequest.getName());
        activity.setContentLink(activityRequest.getContentLink());
        activity.setCourse(course);
        activity.setMaxMark(activityRequest.getMaxMark());
        return activity;
    }

}
