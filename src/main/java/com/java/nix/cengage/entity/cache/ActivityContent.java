package com.java.nix.cengage.entity.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("Activity-content")
public class ActivityContent {

    @Id
    private String link;

    private String description;

    private String content;

}
