package com.java.nix.cengage.entity.dto;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.Role;
import com.java.nix.cengage.entity.User;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProfileDto {
    @EqualsAndHashCode.Exclude
    private Long id;

    private Long courseId;

    private Long userId;

    private Long roleId;

    public static ProfileDto fromProfile(Profile profile) {
        return ProfileDto.builder()
                .id(profile.getId())
                .courseId(profile.getCourse().getId())
                .userId(profile.getUser().getId())
                .roleId(profile.getRole().getId())
                .build();
    }

    public static Profile toProfile(ProfileDto profileDto) {
        Course course = new Course();
        User user = new User();
        Role role = new Role();

        course.setId(profileDto.getCourseId());
        user.setId(profileDto.getUserId());
        role.setId(profileDto.getRoleId());
        return new Profile(course, user, role);
    }
}
