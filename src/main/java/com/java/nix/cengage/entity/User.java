package com.java.nix.cengage.entity;

import com.java.nix.cengage.entity.mapped.Recordable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Getter
@Setter
@Log4j2
@Entity
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = { "username", "email" })
@Where(clause = "is_deleted = false")
@Table(name = "user")
public class User extends Recordable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, nullable = false, unique = true)
    private String username;

    @Email
    @Column(length = 64, nullable = false, unique = true)
    private String email;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @PrePersist
    public void insertUser() {
        this.isDeleted = false;
    }

}
