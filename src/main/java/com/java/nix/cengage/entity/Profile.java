package com.java.nix.cengage.entity;

import com.java.nix.cengage.entity.enums.ProfileState;
import com.java.nix.cengage.entity.mapped.Recordable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

import static com.java.nix.cengage.entity.enums.ProfileRole.TEACHER;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@Table(name = "profile")
@EqualsAndHashCode(callSuper = false,of = {"user","course", "role"})
@Where(clause = "state = 'ACTIVE'")
public class Profile extends Recordable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Course course;

    @ManyToOne
    private User user;

    @ManyToOne
    private Role role;

    @Column
    @Enumerated(EnumType.STRING)
    private ProfileState state;

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinColumn(name = "profile_id", referencedColumnName = "ID")
    private Set<StudentActivity> activities;

    public Profile(Course course, User user, Role role) {
        this.course = course;
        this.user = user;
        this.role = role;
    }

    public static Profile createTeacherProfile(Course course) {
        return Profile.builder()
                .course(course)
                .user(course.getCreator())
                .role(new Role(TEACHER.getId(), TEACHER.getName()))
                .state(ProfileState.ACTIVE)
                .build();
    }

    @PrePersist
    public void prePersist() {
        this.state = ProfileState.ACTIVE;
    }
}
