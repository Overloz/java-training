package com.java.nix.cengage.entity.dto;

import com.java.nix.cengage.entity.cache.ActivityContent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityContentDto {

    private String link;

    private String content;

    private String description;

    public static ActivityContentDto fromActivityContent(ActivityContent activityContent) {
        return new ActivityContentDto(activityContent.getLink(), activityContent.getContent(),
                activityContent.getDescription());
    }

    public static ActivityContent toActivityContent(ActivityContentDto activityContent) {
        return new ActivityContent(activityContent.getLink(), activityContent.getContent(),
                activityContent.getDescription());
    }

}
