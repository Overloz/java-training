package com.java.nix.cengage.entity.dto;

import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.StudentActivity;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentActivityDto {

    @EqualsAndHashCode.Exclude
    private Long id;

    private Long profileId;

    private Long activityId;

    private List<Long> marks;

    private String status;

    public static StudentActivityDto fromStudentActivity(StudentActivity studentActivity) {
        return StudentActivityDto.builder()
                .id(studentActivity.getId())
                .profileId(studentActivity.getProfile().getId())
                .marks(studentActivity.getAttempts().stream()
                        .map(Attempt::getMark)
                        .collect(Collectors.toList()))
                .status(studentActivity.getStatus().getName())
                .build();
    }

}
