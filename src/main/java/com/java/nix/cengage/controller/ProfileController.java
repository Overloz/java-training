package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.dto.ProfileDto;
import com.java.nix.cengage.exception.search.ProfileNotFoundException;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.ProfileValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.java.nix.cengage.entity.enums.ProfileRole.STUDENT;
import static com.java.nix.cengage.entity.enums.ProfileRole.TEACHER;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    private final ProfileService profileService;
    private final ProfileValidationService validationService;

    @Autowired
    public ProfileController(ProfileService profileService,
            ProfileValidationService validationService) {
        this.profileService = profileService;
        this.validationService = validationService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProfileDto> getAll() {
        return profileService.getAll().stream()
                .map(ProfileDto::fromProfile)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProfileDto getById(@PathVariable Long id) {
        Profile profile = Optional.ofNullable(profileService.getById(id))
                .orElseThrow(() -> new ProfileNotFoundException(id));

        return ProfileDto.fromProfile(profile);
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ProfileDto> getByUserId(@PathVariable Long id) {
        return profileService.findProfilesOfGivenUser(id).stream()
                .map(ProfileDto::fromProfile)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id) {
        profileService.deleteById(id);
    }

    @PostMapping("/teacher")
    @ResponseStatus(HttpStatus.CREATED)
    public ProfileDto createTeacher(@RequestBody ProfileDto profileDto) {
        profileDto.setRoleId(TEACHER.getId());
        Profile profile = profileService.insert(ProfileDto.toProfile(profileDto));
        return ProfileDto.fromProfile(profile);
    }

    @PostMapping("/student")
    @ResponseStatus(HttpStatus.CREATED)
    public ProfileDto createStudent(@RequestBody ProfileDto profileDto) {
        profileDto.setRoleId(STUDENT.getId());
        Profile profile = profileService.insertStudent(ProfileDto.toProfile(profileDto));
        return ProfileDto.fromProfile(profile);
    }

    @GetMapping("/teacher/{teacherId}/course/{courseId}")
    @ResponseStatus(HttpStatus.OK)
    public List<ProfileDto> getAllStudents(@PathVariable Long teacherId,
            @PathVariable Long courseId) {
        validationService.checkIfUserIsTeacherOnGivenCourse(teacherId, courseId);
        return profileService.findStudentsOfGivenCourse(courseId).stream()
                .map(ProfileDto::fromProfile)
                .collect(Collectors.toList());
    }

}
