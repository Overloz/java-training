package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.entity.dto.UserDto;
import com.java.nix.cengage.exception.search.UserNotFoundException;
import com.java.nix.cengage.service.UserService;
import com.java.nix.cengage.service.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserValidationService userValidationService;
    private final UserService userService;

    @Autowired
    public UserController(UserValidationService userValidationService, UserService userService) {
        this.userValidationService = userValidationService;
        this.userService = userService;
    }

    @GetMapping
    public List<UserDto> getAll() {
        return userService.getAll().stream()
                .map(UserDto::fromUser)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getById(@PathVariable Long id) {
        User user = Optional.of(userService.getById(id))
                .orElseThrow(() -> new UserNotFoundException(id));
        return UserDto.fromUser(user);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id) {
        userService.deleteById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto createUser(@RequestBody UserDto request) {
        User user = UserDto.toUser(request);
        userValidationService.checkThatUserNotExists(user);
        user = userService.saveUser(user);
        return UserDto.fromUser(user);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody UserDto request) {
        userService.update(UserDto.toUser(request));
    }
}
