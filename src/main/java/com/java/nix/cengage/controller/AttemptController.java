package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.dto.AttemptDto;
import com.java.nix.cengage.service.AttemptService;
import com.java.nix.cengage.service.AttemptValidationService;
import com.java.nix.cengage.service.StudentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.java.nix.cengage.entity.enums.ActivityStatus.COMPLETED;

@RestController
@RequestMapping("/attempt")
public class AttemptController {

    private final StudentActivityService studentActivityService;
    private final AttemptValidationService validationService;
    private final AttemptService attemptService;

    @Autowired
    public AttemptController(StudentActivityService studentActivityService,
            AttemptValidationService validationService,
            AttemptService attemptService) {
        this.studentActivityService = studentActivityService;
        this.validationService = validationService;
        this.attemptService = attemptService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<AttemptDto> getAll() {
        return attemptService.getAll().stream()
                .map(AttemptDto::fromAttempt)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<AttemptDto> getAllByStudentActivityId(@PathVariable Long id) {
        return attemptService.getAllByStudentActivityId(id).stream()
                .map(AttemptDto::fromAttempt)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AttemptDto createAttempt(@RequestBody AttemptDto attemptDto) {
        validationService.checkThatAttemptNotInProgressAndNotFinalized(attemptDto.getStudentActivityId());
        Attempt attempt = attemptService.insert(AttemptDto.toAttempt(attemptDto));
        return AttemptDto.fromAttempt(attempt);
    }

    @PutMapping("/activity/{activityId}/profile/{profileId}")
    public void finalizeAttempt(@PathVariable Long activityId, @PathVariable Long profileId){
        studentActivityService.updateStatusByActivityIdAndProfileId(COMPLETED,activityId, profileId);
    }

}
