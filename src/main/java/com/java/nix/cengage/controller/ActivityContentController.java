package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.cache.ActivityContent;
import com.java.nix.cengage.entity.dto.ActivityContentDto;
import com.java.nix.cengage.exception.search.ActivityContentNotFoundException;
import com.java.nix.cengage.exception.search.ActivityNotFoundException;
import com.java.nix.cengage.service.ActivityContentService;
import com.java.nix.cengage.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/activitycontent")
public class ActivityContentController {

    private final ActivityService activityService;
    private final ActivityContentService activityContentService;

    @Autowired
    public ActivityContentController(ActivityService activityService,
            ActivityContentService activityContentService) {
        this.activityService = activityService;
        this.activityContentService = activityContentService;
    }

    @GetMapping("/activity/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActivityContentDto getMetaInfo(@PathVariable Long id) {
        Activity activity = Optional.ofNullable(activityService.getById(id))
                .orElseThrow(() -> new ActivityNotFoundException(id));
        ActivityContent activityContent = Optional
                .ofNullable(activityContentService.getInfoByLink(activity.getContentLink()))
                .orElseThrow(() -> new ActivityContentNotFoundException(activity.getContentLink()));

        return ActivityContentDto.fromActivityContent(activityContent);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ActivityContentDto activityContentDto) {
        activityContentService.update(ActivityContentDto.toActivityContent(activityContentDto));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ActivityContentDto insert(@RequestBody ActivityContentDto request) {
        ActivityContent activityContent = ActivityContentDto.toActivityContent(request);
        activityContent = activityContentService.insert(activityContent);
        return ActivityContentDto.fromActivityContent(activityContent);
    }
}
