package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.dto.ActivityDto;
import com.java.nix.cengage.exception.search.ActivityNotFoundException;
import com.java.nix.cengage.exception.search.CourseNotFoundException;
import com.java.nix.cengage.service.ActivityService;
import com.java.nix.cengage.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/activity")
public class ActivityController {

    private final CourseService courseService;
    private final ActivityService activityService;

    @Autowired
    public ActivityController(CourseService courseService, ActivityService activityService) {
        this.courseService = courseService;
        this.activityService = activityService;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ActivityDto getById(@PathVariable Long id) {
        Activity activity = Optional.ofNullable(activityService.getById(id))
                .orElseThrow(() -> new ActivityNotFoundException(id));

        return ActivityDto.fromActivity(activity);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ActivityDto> getAll() {
        return activityService.getAll().stream()
                .map(ActivityDto::fromActivity)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ActivityDto addActivity(@RequestBody ActivityDto activityRequest) {
        Course course = Optional.ofNullable(courseService.getById(activityRequest.getCourseId()))
                .orElseThrow(() -> new CourseNotFoundException(activityRequest.getCourseId()));
        Activity activity = ActivityDto.toActivity(activityRequest);
        activity.setCourse(course);
        activity = activityService.insert(activity);
        return ActivityDto.fromActivity(activity);
    }


    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ActivityDto request) {
        activityService.update(ActivityDto.toActivity(request));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id) {
        activityService.deleteById(id);
    }

    @GetMapping("/course/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Set<ActivityDto> getActivitiesByCourse(@PathVariable Long id) {
        return activityService.getActivitiesByCourse(id).stream()
                .map(ActivityDto::fromActivity)
                .collect(Collectors.toSet());
    }
}
