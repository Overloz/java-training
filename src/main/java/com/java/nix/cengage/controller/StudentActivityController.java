package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.dto.StudentActivityDto;
import com.java.nix.cengage.service.StudentActivityService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/studentActivity")
public class StudentActivityController {

    private final StudentActivityService studentActivityService;

    public StudentActivityController(StudentActivityService studentActivityService) {
        this.studentActivityService = studentActivityService;
    }

    @GetMapping("/activity/{id}")
    public List<StudentActivityDto> getAllByActivityId(@PathVariable Long id){
        return studentActivityService.getAllByActivityId(id).stream()
                .map(StudentActivityDto::fromStudentActivity)
                .collect(Collectors.toList());
    }

    @PutMapping("/activity/{id}")
    public void finalizeAllByActivityId(@PathVariable Long id){
        studentActivityService.finalizeAllByActivityId(id);
    }

    @GetMapping("/{courseId}/student/{studentId}")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentActivityDto> getActivitiesWithMarks(@PathVariable Long courseId,
            @PathVariable Long studentId) {
        return studentActivityService.getResults(studentId, courseId).stream()
                .map(StudentActivityDto::fromStudentActivity)
                .collect(Collectors.toList());
    }
}
