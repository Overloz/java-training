package com.java.nix.cengage.controller;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.dto.CourseDto;
import com.java.nix.cengage.service.CourseService;
import com.java.nix.cengage.service.CourseValidationService;
import com.java.nix.cengage.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/course")
public class CourseController {

    private final CourseValidationService courseValidationService;
    private final ProfileService profileService;
    private final CourseService courseService;

    @Autowired
    public CourseController(CourseValidationService courseValidationService,
            ProfileService profileService, CourseService courseService) {
        this.courseValidationService = courseValidationService;
        this.profileService = profileService;
        this.courseService = courseService;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CourseDto getById(@PathVariable Long id) {
        Course course = courseService.getById(id);
        return CourseDto.fromCourse(course);
    }

    @GetMapping("/external/{externalId}")
    @ResponseStatus(HttpStatus.OK)
    public CourseDto getByExternalId(@PathVariable String externalId) {
        Course course = courseService.getByExternalId(externalId);
        return CourseDto.fromCourse(course);
    }

    @GetMapping
    public List<CourseDto> getAll() {
        return courseService.getAll().stream()
                .map(CourseDto::fromCourse)
                .collect(Collectors.toList());
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Set<CourseDto> getCoursesOfStudent(@PathVariable Long id) {
        return courseService.findAllCoursesForStudent(id).stream()
                .map(CourseDto::fromCourse)
                .collect(Collectors.toSet());
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody CourseDto request) {
        courseService.update(CourseDto.toCourse(request));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id) {
        courseService.deleteById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CourseDto create(@RequestBody CourseDto request) {
        courseValidationService.checkIfTeacherAlreadyHasCourseWithGivenName(request.getTeacherId(),
                request.getName());
        Course course = CourseDto.toCourse(request);
        course = courseService.insert(course);
        profileService.insert(Profile.createTeacherProfile(course));
        return CourseDto.fromCourse(course);
    }
}
