package com.java.nix.cengage.exception.validation;

public class AttemptValidationException extends ValidationException {
    public AttemptValidationException(String message) {
        super(message);
    }
}
