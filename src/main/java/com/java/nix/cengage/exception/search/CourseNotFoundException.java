package com.java.nix.cengage.exception.search;

public class CourseNotFoundException extends NotFoundException {
    public CourseNotFoundException(Long id) {
        super(String.format("Course with id: %d not found", id));
    }
}
