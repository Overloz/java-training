package com.java.nix.cengage.exception.validation;

public class UserValidationException extends ValidationException {
    public UserValidationException(String message) {
        super(message);
    }
}

