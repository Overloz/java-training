package com.java.nix.cengage.exception.validation;

public class ProfileValidationException extends ValidationException {
    public ProfileValidationException(String message) {
        super(message);
    }
}
