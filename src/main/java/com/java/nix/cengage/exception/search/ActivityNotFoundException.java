package com.java.nix.cengage.exception.search;

public class ActivityNotFoundException extends NotFoundException {
    public ActivityNotFoundException(Long id) {
        super(String.format("Activity with id: %d not found", id));
    }
}
