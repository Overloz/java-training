package com.java.nix.cengage.exception.search;

public class ProfileNotFoundException extends NotFoundException {
    public ProfileNotFoundException(Long id) {
        super(String.format("Profile with id: %d not found", id));
    }
}
