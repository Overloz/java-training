package com.java.nix.cengage.exception.search;

public class StudentActivityNotFoundException extends NotFoundException{
    public StudentActivityNotFoundException(Long id) {
        super(String.format("StudentActivity with id: %d not found",id));
    }
}
