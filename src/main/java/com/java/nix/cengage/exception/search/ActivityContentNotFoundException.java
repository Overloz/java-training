package com.java.nix.cengage.exception.search;

public class ActivityContentNotFoundException extends NotFoundException {
    public ActivityContentNotFoundException(String link) {
        super(String.format("Content of activity with link: %s not found", link));
    }
}
