package com.java.nix.cengage.exception.handler;

import com.java.nix.cengage.entity.dto.ErrorDto;
import com.java.nix.cengage.exception.validation.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ValidationExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorDto> exceptionHandlerUserExists(ValidationException exception) {
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        ErrorDto errorDto = new ErrorDto(1001, System.currentTimeMillis(), exception.getMessage());

        return new ResponseEntity<>(errorDto, httpStatus);
    }

}