package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.Role;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.exception.search.CourseNotFoundException;
import com.java.nix.cengage.exception.search.ProfileNotFoundException;
import com.java.nix.cengage.exception.search.RoleNotFoundException;
import com.java.nix.cengage.exception.search.UserNotFoundException;
import com.java.nix.cengage.repositories.mysql.ProfileRepository;
import com.java.nix.cengage.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProfileServiceImpl implements ProfileService {

    private RoleService roleService;
    private UserService userService;
    private CourseService courseService;
    private StudentActivityService studentActivityService;
    private ProfileRepository profileRepository;

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }
    @Autowired
    public void setStudentActivityService(StudentActivityService studentActivityService) {
        this.studentActivityService = studentActivityService;
    }
    @Autowired
    public void setProfileRepository(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    public List<Profile> findStudentsOfGivenCourse(Long id) {
        return profileRepository.findAllStudentsByCourseId(id);
    }

    @Override
    public List<Profile> findProfilesOfGivenUser(Long id) {
        return profileRepository.findAllByUserId(id);
    }

    @Override
    public List<Profile> getAll() {
        return profileRepository.findAll();
    }

    @Override
    public Profile insert(Profile profile) {
        Long userId = profile.getUser().getId();
        Long courseId = profile.getCourse().getId();
        Long roleId = profile.getRole().getId();

        profile = populateProfile(userId,courseId, roleId);
        return profileRepository.save(profile);
    }

    @Override
    @Transactional
    public Profile insertStudent(Profile profile) {
        Profile saved = insert(profile);
        studentActivityService.addActivitiesToStudent(saved);
        return saved;
    }

    private Profile populateProfile(Long userId, Long courseId, Long roleId) {
        Course course = Optional.ofNullable(courseService.getById(courseId))
                .orElseThrow(() -> new CourseNotFoundException(courseId));
        User user = Optional.ofNullable(userService.getById(userId))
                .orElseThrow(() -> new UserNotFoundException(userId));
        Role role = Optional.ofNullable(roleService.findById(roleId))
                .orElseThrow(() -> new RoleNotFoundException(roleId));

        return new Profile(course, user, role);
    }

    @Override
    public Profile getById(Long id) {
        return profileRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Profile profile) {
        profileRepository.save(profile);
    }

    @Override
    public void deleteById(Long id) {
        if (!profileRepository.existsById(id)) {
            throw new ProfileNotFoundException(id);
        }

        profileRepository.deleteProfileById(id);
    }

    @Override
    public boolean existsById(Long id) {
        return profileRepository.existsById(id);
    }

    @Override
    public void deleteAllByUserId(Long id) {
        profileRepository.deleteAllByUserId(id);
    }

    @Override
    public void deleteAllByCourseId(Long id) {
        profileRepository.deleteAllByCourseId(id);
    }

    @Override
    public boolean existsByRoleIdAndUserIdAndCourseId(Long roleId, Long userId, Long courseId) {
        return profileRepository.existsByUserIdAndRoleIdAndCourseId(userId, roleId, courseId);
    }
}
