package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.cache.Cache;
import com.java.nix.cengage.repositories.mongo.CacheRepository;
import com.java.nix.cengage.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class CacheServiceImpl implements CacheService {

    private final CacheRepository cacheRepository;

    @Autowired
    public CacheServiceImpl(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    @Override
    public void delete(Long courseId) {
        cacheRepository.deleteById(courseId);
    }

    @Override
    public void insert(Set<Activity> activities, Long courseId) {
        Optional<Cache> existing = cacheRepository.findById(courseId);
        if (existing.isEmpty()) {
            cacheRepository.insert(new Cache(courseId, activities));
        }
    }

    @Override
    public Optional<Cache> find(Long courseId) {
        return cacheRepository.findById(courseId);
    }
}
