package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Attempt;

import java.util.List;

public interface AttemptService {

    List<Attempt> getAll();

    Attempt getById(Long id);

    List<Attempt> getAllByStudentActivityId(Long id);

    Attempt insert(Attempt attempt);

}
