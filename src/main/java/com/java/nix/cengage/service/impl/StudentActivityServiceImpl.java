package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.entity.enums.ActivityStatus;
import com.java.nix.cengage.repositories.mysql.StudentActivityRepository;
import com.java.nix.cengage.service.StudentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;

import static com.java.nix.cengage.entity.enums.ActivityStatus.FINALIZED;

@Service
public class StudentActivityServiceImpl implements StudentActivityService {

    private final StudentActivityRepository studentActivityRepository;

    @Autowired
    public StudentActivityServiceImpl(StudentActivityRepository studentActivityRepository) {
        this.studentActivityRepository = studentActivityRepository;
    }

    @Override
    public List<StudentActivity> getResults(Long studentId, Long courseId) {
        return studentActivityRepository.findAllByProfileIdAndActivityCourseId(studentId, courseId);
    }

    @Override
    @Transactional
    public void addActivitiesToStudent(Profile profile) {
        Set<Activity> activities = profile.getCourse().getActivities();
        activities.forEach(activity -> studentActivityRepository
                .save(StudentActivity.studentActivityBuilder(activity, profile)));
    }

    @Override
    @Transactional
    public void addActivityToStudents(Activity activity, List<Profile> students) {
        students.forEach(profile -> studentActivityRepository
                .save(StudentActivity.studentActivityBuilder(activity, profile)));
    }

    @Override
    @Transactional
    public void updateStatusByActivityIdAndProfileId(ActivityStatus activityStatus, Long activityId,
            Long profileId) {
        studentActivityRepository
                .updateStatusByActivityIdProfileId(activityStatus.getName(), activityId, profileId);
    }

    @Override
    public StudentActivity getById(Long id) {
        return studentActivityRepository.findById(id).orElse(null);
    }

    @Override
    public List<StudentActivity> getAllByActivityId(Long activityId) {
        return studentActivityRepository.findAllByActivityId(activityId);
    }

    @Override
    @Transactional
    public void finalizeAllByActivityId(Long activityId) {
        setFinalMarkInStudentActivities(getAllByActivityId(activityId));
        studentActivityRepository.finalizeAllByActivityId(FINALIZED.getName(), activityId);
    }

    private void setFinalMarkInStudentActivities(List<StudentActivity> studentActivities) {
        for (StudentActivity studentActivity : studentActivities) {
            studentActivity.setFinalMark(getBiggestMarkFromAttempts(studentActivity));
            studentActivityRepository.save(studentActivity);
        }
    }

    private Long getBiggestMarkFromAttempts(StudentActivity studentActivity) {
        long failMark = 0L;
        List<Attempt> attempts = studentActivity.getAttempts();
        return CollectionUtils.isEmpty(attempts)
                ? failMark
                : attempts.stream()
                        .mapToLong(Attempt::getMark)
                        .max().orElse(failMark);
    }
}
