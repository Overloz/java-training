package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.repositories.mysql.CourseRepository;
import com.java.nix.cengage.service.CourseValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseValidationServiceImpl implements CourseValidationService {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseValidationServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public boolean checkIfTeacherAlreadyHasCourseWithGivenName(Long teacherId, String courseName) {
        return courseRepository.existsByNameAndCreatorId(courseName,teacherId);
    }
}
