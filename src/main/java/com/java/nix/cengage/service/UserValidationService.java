package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.User;

public interface UserValidationService {

    void checkThatUserNotExists(User user);

}
