package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Course;

import java.util.List;

public interface CourseService {

    List<Course> getAll();

    Course insert(Course course);

    List<Course> findAllCoursesForStudent(Long studentId);

    Course getById(Long id);

    void update(Course course);

    void deleteById(Long id);

    boolean existsById(Long id);

    Course getByExternalId(String externalId);
}
