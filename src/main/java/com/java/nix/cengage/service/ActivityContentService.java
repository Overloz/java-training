package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.cache.ActivityContent;

public interface ActivityContentService {
    ActivityContent getInfoByLink(String link);

    ActivityContent insert(ActivityContent activityContent);

    void update(ActivityContent activityContent);
}
