package com.java.nix.cengage.service;

public interface CourseValidationService {

    boolean checkIfTeacherAlreadyHasCourseWithGivenName(Long teacherId, String courseName);

}
