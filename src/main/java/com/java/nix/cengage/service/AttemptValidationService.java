package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Attempt;

public interface AttemptValidationService {

    void checkIfMarkIsNotNegativeOrNotBiggerThanMax(Attempt attempt);

    void checkThatAttemptNotInProgressAndNotFinalized(Long id);

}
