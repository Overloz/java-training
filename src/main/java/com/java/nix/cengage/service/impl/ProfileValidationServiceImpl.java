package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.enums.ProfileRole;
import com.java.nix.cengage.exception.validation.ProfileValidationException;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.ProfileValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileValidationServiceImpl implements ProfileValidationService {

    private final ProfileService profileService;

    @Autowired
    public ProfileValidationServiceImpl(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Override
    public void checkIfUserIsTeacherOnGivenCourse(Long userId, Long courseId) {
        if (!profileService.existsByRoleIdAndUserIdAndCourseId(ProfileRole.TEACHER.getId(), userId,
                courseId)) {
            throw new ProfileValidationException(
                    String.format("Given user: %d doesn't have teacher role on course: %d ", userId,
                            courseId));
        }
    }

}
