package com.java.nix.cengage.service;

public interface ProfileValidationService {

    void checkIfUserIsTeacherOnGivenCourse(Long userId, Long courseId);

}
