package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.cache.Cache;
import com.java.nix.cengage.entity.cache.CachedActivity;
import com.java.nix.cengage.exception.search.ActivityNotFoundException;
import com.java.nix.cengage.repositories.mysql.ActivityRepository;
import com.java.nix.cengage.service.ActivityService;
import com.java.nix.cengage.service.CacheService;
import com.java.nix.cengage.service.ProfileService;
import com.java.nix.cengage.service.StudentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ActivityServiceImpl implements ActivityService {

    private StudentActivityService studentActivityService;
    private ProfileService profileService;
    private ActivityRepository activityRepository;
    private CacheService cacheService;

    @Autowired
    public void setStudentActivityService(StudentActivityService studentActivityService) {
        this.studentActivityService = studentActivityService;
    }
    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }
    @Autowired
    public void setActivityRepository(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }
    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public List<Activity> getAll() {
        return activityRepository.findAll();
    }

    @Override
    @Transactional
    public Activity insert(Activity activity) {
        Activity savedActivity = activityRepository.save(activity);
        List<Profile> students = profileService
                .findStudentsOfGivenCourse(activity.getCourse().getId());
        studentActivityService.addActivityToStudents(activity, students);
        cacheService.delete(activity.getCourse().getId());
        return savedActivity;
    }

    @Override
    public Activity getById(Long id) {
        return activityRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void update(Activity activity) {
        Activity existingActivity = activityRepository.findById(activity.getId())
                .orElseThrow(() -> new ActivityNotFoundException(activity.getId()));
        updateActivity(existingActivity, activity);
    }

    private void updateActivity(Activity existingActivity, Activity activity) {
        existingActivity.setName(activity.getName());
        existingActivity.setContentLink(activity.getContentLink());
        activityRepository.save(existingActivity);
        cacheService.delete(existingActivity.getCourse().getId());
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        Activity existing = activityRepository.findById(id)
                .orElseThrow(() -> new ActivityNotFoundException(id));

        activityRepository.deleteActivityById(id);
        cacheService.delete(existing.getCourse().getId());
    }

    @Override
    public void deleteAllByCourseId(Long courseId) {
        activityRepository.deleteAllByCourseId(courseId);
    }

    @Override
    public Set<Activity> getActivitiesByCourse(Long courseId) {
        Optional<Cache> cache = cacheService.find(courseId);

        if (cache.isPresent()) {
            return getActivitiesSavedInCache(cache.get().getActivitySet());
        } else {
            return getActivitiesFromDataBase(courseId);
        }

    }

    private Set<Activity> getActivitiesSavedInCache(Set<CachedActivity> activities){
        return activities.stream()
                .map(CachedActivity::fromCachedActivity)
                .collect(Collectors.toSet());
    }

    private Set<Activity> getActivitiesFromDataBase(Long courseId){
        Set<Activity> activities = activityRepository.findAllByCourseId(courseId);
        cacheService.insert(activities, courseId);
        return activities;
    }

    @Override
    public boolean existsById(Long id) {
        return activityRepository.existsById(id);
    }
}
