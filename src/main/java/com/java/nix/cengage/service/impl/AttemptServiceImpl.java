package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.repositories.mysql.AttemptRepository;
import com.java.nix.cengage.service.AttemptService;
import com.java.nix.cengage.service.AttemptValidationService;
import com.java.nix.cengage.service.StudentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttemptServiceImpl implements AttemptService {

    private final AttemptValidationService validationService;
    private final AttemptRepository attemptRepository;
    private final StudentActivityService studentActivityService;

    @Autowired
    public AttemptServiceImpl(AttemptValidationService validationService,
            AttemptRepository attemptRepository, StudentActivityService studentActivityService) {
        this.validationService = validationService;
        this.attemptRepository = attemptRepository;
        this.studentActivityService = studentActivityService;
    }

    @Override
    public List<Attempt> getAll() {
        return attemptRepository.findAll();
    }

    @Override
    public Attempt getById(Long id) {
        return attemptRepository.findById(id).orElse(null);
    }

    @Override
    public List<Attempt> getAllByStudentActivityId(Long id) {
        return attemptRepository.findAllByStudentActivityId(id);
    }

    @Override
    public Attempt insert(Attempt attempt) {
        populateAttempt(attempt);
        validationService.checkIfMarkIsNotNegativeOrNotBiggerThanMax(attempt);
        return attemptRepository.save(attempt);
    }

    private void populateAttempt(Attempt attempt) {
        attempt.setStudentActivity(
                studentActivityService.getById(attempt.getStudentActivity().getId()));
    }
}
