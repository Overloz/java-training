package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.cache.Cache;

import java.util.Optional;
import java.util.Set;

public interface CacheService {
    void delete(Long courseId);

    void insert(Set<Activity> courseActivity, Long courseId);

    Optional<Cache> find(Long courseId);
}
