package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Activity;

import java.util.List;
import java.util.Set;

public interface ActivityService {
    List<Activity> getAll();

    Activity insert(Activity activity);

    Activity getById(Long id);

    void update(Activity activity);

    void deleteById(Long id);

    void deleteAllByCourseId(Long courseId);

    Set<Activity> getActivitiesByCourse(Long courseId);

    boolean existsById(Long id);
}
