package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Activity;
import com.java.nix.cengage.entity.Profile;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.entity.enums.ActivityStatus;

import java.util.List;

public interface StudentActivityService {

    List<StudentActivity> getResults(Long studentId, Long courseId);

    void addActivitiesToStudent(Profile profile);

    void addActivityToStudents(Activity activity, List<Profile> students);

    void updateStatusByActivityIdAndProfileId(ActivityStatus activityStatus, Long activityId, Long profileId);

    StudentActivity getById(Long id);

    List<StudentActivity> getAllByActivityId(Long activityId);

    void finalizeAllByActivityId(Long activityId);
}
