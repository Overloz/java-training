package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.exception.validation.UserValidationException;
import com.java.nix.cengage.service.UserService;
import com.java.nix.cengage.service.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserValidationServiceImpl implements UserValidationService {

    private final UserService userService;

    @Autowired
    public UserValidationServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkThatUserNotExists(User user) {
        if (userService.existsByUsername(user.getUsername())) {
            throw new UserValidationException(
                    String.format("User with username: %s already exist", user.getUsername()));
        } else if (userService.existsByEmail(user.getEmail())) {
            throw new UserValidationException(
                    String.format("User with email: %s already exist", user.getEmail()));
        }
    }
}
