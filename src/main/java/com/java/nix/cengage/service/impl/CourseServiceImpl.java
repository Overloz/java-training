package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Course;
import com.java.nix.cengage.entity.User;
import com.java.nix.cengage.exception.search.CourseNotFoundException;
import com.java.nix.cengage.exception.search.UserNotFoundException;
import com.java.nix.cengage.repositories.mysql.CourseRepository;
import com.java.nix.cengage.service.*;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CourseServiceImpl implements CourseService {

    private CacheService cacheService;
    private ActivityService activityService;
    private CourseRepository courseRepository;
    private ProfileService profileService;
    private UserService userService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    @Autowired
    public void setCourseRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<Course> getAll() {
        return courseRepository.findAll();
    }

    @Override
    public Course insert(Course course) {
        Course populatedCourse = populateCourse(course);
        return courseRepository.save(populatedCourse);
    }

    private Course populateCourse(Course course) {
        Long teacherId = course.getCreator().getId();
        User user = Optional.ofNullable(userService.getById(teacherId))
                .orElseThrow(() -> new UserNotFoundException(teacherId));
        course.setCreator(user);
        course.setExternalId(UUID.randomUUID().toString());
        return course;
    }

    @Override
    public List<Course> findAllCoursesForStudent(Long userId) {
        return courseRepository.findAllByStudentsId(userId);
    }

    @Override
    public Course getById(Long id) {
        return courseRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Course course) {
        Course existing = courseRepository.findById(course.getId())
                .orElseThrow(() -> new CourseNotFoundException(course.getId()));

        existing.setName(course.getName());
        courseRepository.save(existing);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (!courseRepository.existsById(id)) {
            throw new CourseNotFoundException(id);
        }

        activityService.deleteAllByCourseId(id);
        profileService.deleteAllByCourseId(id);
        courseRepository.deleteCourseById(id);
        cacheService.delete(id);
    }

    @Override
    public boolean existsById(Long id) {
        return courseRepository.existsById(id);
    }

    @Override
    public Course getByExternalId(String externalId) {
        return courseRepository.findByExternalId(externalId);
    }

}
