package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Profile;

import java.util.List;

public interface ProfileService {

    List<Profile> findStudentsOfGivenCourse(Long id);

    List<Profile> findProfilesOfGivenUser(Long id);

    List<Profile> getAll();

    Profile insert(Profile profile);

    Profile insertStudent(Profile profile);

    Profile getById(Long id);

    void update(Profile profile);

    void deleteById(Long id);

    void deleteAllByUserId(Long id);

    boolean existsById(Long id);

    boolean existsByRoleIdAndUserIdAndCourseId(Long roleId, Long userId, Long courseId);

    void deleteAllByCourseId(Long id);
}
