package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.Role;

public interface RoleService {

    Role insert(Role role);

    Role findById(Long id);

}
