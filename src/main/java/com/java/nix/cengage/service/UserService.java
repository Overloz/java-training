package com.java.nix.cengage.service;

import com.java.nix.cengage.entity.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User saveUser(User user);

    User getById(Long id);

    void update(User user);

    void deleteById(Long id);

    boolean existsById(Long id);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);
}
