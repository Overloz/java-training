package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.cache.ActivityContent;
import com.java.nix.cengage.exception.search.ActivityContentNotFoundException;
import com.java.nix.cengage.repositories.mongo.ActivityContentRepository;
import com.java.nix.cengage.service.ActivityContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityContentServiceImpl implements ActivityContentService {

    private final ActivityContentRepository activityContentRepository;

    @Autowired
    public ActivityContentServiceImpl(ActivityContentRepository activityContentRepository) {
        this.activityContentRepository = activityContentRepository;
    }

    @Override
    public ActivityContent getInfoByLink(String link) {
        return activityContentRepository.findByLink(link).orElse(null);
    }

    @Override
    public ActivityContent insert(ActivityContent activityContent) {
        return activityContentRepository.save(activityContent);
    }

    @Override
    public void update(ActivityContent activityContent) {
        ActivityContent existing = activityContentRepository.findByLink(activityContent.getLink())
                .orElseThrow(() -> new ActivityContentNotFoundException(activityContent.getLink()));
        existing.setContent(activityContent.getContent());
        existing.setDescription(activityContent.getDescription());
        activityContentRepository.save(existing);
    }
}
