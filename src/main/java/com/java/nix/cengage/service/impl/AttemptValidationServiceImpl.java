package com.java.nix.cengage.service.impl;

import com.java.nix.cengage.entity.Attempt;
import com.java.nix.cengage.entity.StudentActivity;
import com.java.nix.cengage.exception.search.StudentActivityNotFoundException;
import com.java.nix.cengage.exception.validation.AttemptValidationException;
import com.java.nix.cengage.service.AttemptValidationService;
import com.java.nix.cengage.service.StudentActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.java.nix.cengage.entity.enums.ActivityStatus.FINALIZED;
import static com.java.nix.cengage.entity.enums.ActivityStatus.IN_PROGRESS;

@Service
public class AttemptValidationServiceImpl implements AttemptValidationService {

    private final StudentActivityService studentActivityService;

    @Autowired
    public AttemptValidationServiceImpl(StudentActivityService studentActivityService) {
        this.studentActivityService = studentActivityService;
    }

    @Override
    public void checkIfMarkIsNotNegativeOrNotBiggerThanMax(Attempt attempt) {
        if (attempt.getMark() > attempt.getStudentActivity().getActivity().getMaxMark()) {
            throw new AttemptValidationException(
                    "Invalid mark: %d is bigger than possible max mark");
        } else if (attempt.getMark() < 0) {
            throw new AttemptValidationException("Invalid mark: %d is less than zero");
        }
    }

    @Override
    public void checkThatAttemptNotInProgressAndNotFinalized(Long id) {
        StudentActivity studentActivity = studentActivityService.getById(id);

        if (studentActivity == null) {
            throw new StudentActivityNotFoundException(id);
        } else if (studentActivity.getStatus() == IN_PROGRESS) {
            throw new AttemptValidationException("There currently an attempt in progress");
        } else if (studentActivity.getStatus() == FINALIZED) {
            throw new AttemptValidationException("Student activity has been finalized");
        }
    }

}
