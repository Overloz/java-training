package com.java.nix.cengage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CengageApplication {

    public static void main(String[] args) {
        SpringApplication.run(CengageApplication.class, args);
    }

}
